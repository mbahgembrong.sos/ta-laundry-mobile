# Android Framework 
This App using Kotlin  JDK 11

## Official Android Documentation

Documentation for the framework can be found on the [Android website](https://developer.android.com/docs).

# To use this app

- Clone project from this repository

- Open Using Intellij IDEA or Android Studio
  
- Open [Mapbox](https://account.mapbox.com/) Account and get your access token 
  
- Paste Token to [gradle.properties](gradle.properties) file and [Mapbox Acces Token Values](./app/src/main/res/values/mapbox_access_token.xml)
  
- Open [Firebase](https://console.firebase.google.com/) and active FCM
  
- download `google-services.json` and paste to [app](./app) folder
  
- paste URL server website to variabel `URL` in [AppConstants.kt](app/src/main/java/com/example/detergentapp/utils/AppConstants.kt)

- open emulator and run project

- Done.
  
### Dependency
  - Mapbox
  - Firebase
  - MVVM
  - Retrofit
  - QR Code
  - Camera X
  - Timber
  - Picaso
  - Circle Image View
