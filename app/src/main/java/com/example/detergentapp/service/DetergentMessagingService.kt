package com.example.detergentapp.service


import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import com.example.detergentapp.R
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.TransaksiService
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.ResponseData
import com.example.detergentapp.entity.Transaksi
import com.example.detergentapp.entity.Ulasan
import com.example.detergentapp.ui.auth.AuthViewModel
import com.example.detergentapp.ui.main.MainActivity
import com.example.detergentapp.ui.main.order.OrderDetailActivity
import com.example.detergentapp.utils.AppConstants.INTENT_TRANSAKSI_ID
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.internal.notify
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class DetergentMessagingService : FirebaseMessagingService() {

    val RC_INTENT = 100
    val CHANNEL_ID = "detergentlaundry"
    lateinit var viewModel: AuthViewModel
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        Timber.i("New token: ${token.toString()}")
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        Timber.i("Message received: ${p0}")

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        if (p0.data.isNotEmpty()) {
            Timber.i("Message data: ${p0.data}")
            p0.data.let {
                val payload = JSONObject(it["payload"])
                val intent = Intent(this, OrderDetailActivity::class.java)
                var transaksi: Transaksi? = null
                if (payload.has("transaksi")) {
                    val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
                    val call = retroInstance.getShowTransaksi(
                        payload.getString("transaksi")
                    )
                    call.enqueue(object : Callback<ResponseData<Transaksi>?> {
                        override fun onResponse(
                            call: Call<ResponseData<Transaksi>?>,
                            response: Response<ResponseData<Transaksi>?>
                        ) {
                            if (response.isSuccessful) {
                                transaksi = response.body()?.data
                                sendNotif(
                                    it.getValue("title"),
                                    it.getValue("body"),
                                    intent.putExtra(OrderDetailActivity.DETAIL_TRACKING, transaksi)
                                )
                            } else {
                                transaksi
                            }
                        }

                        override fun onFailure(call: Call<ResponseData<Transaksi>?>, t: Throwable) {
                            transaksi = null
                        }
                    })
                }

            }
        }
    }

    fun getTransaksi(json: String): Transaksi? {
        val listType = object : TypeToken<Transaksi?>() {}.type
        return Gson().fromJson(json, listType)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun sendNotif(title: String, body: String, intent: Intent) {
        val pendingIntent = PendingIntent.getActivity(
            this, RC_INTENT, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        val audioAttributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                CHANNEL_ID, "detergentlanundry",
                NotificationManager.IMPORTANCE_HIGH
            )
            mChannel.description = "detergentlanundry"
            mChannel.setSound(ringtoneUri, audioAttributes)
            notificationManager.createNotificationChannel(mChannel)

        }
        val notificationBuilder = NotificationCompat.Builder(baseContext, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setContentText(body)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(body)
            )
            .setSound(ringtoneUri)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true).build()
        notificationManager.notify(RC_INTENT, notificationBuilder)
    }
}
