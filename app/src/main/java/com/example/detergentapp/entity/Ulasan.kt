package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ulasan(
    @SerializedName("id_transaksi") var idTransaksi: String?,
    @SerializedName("ulasan") var ulasan: String?,
    @SerializedName("star") var star: Double?,
    @SerializedName("id_outlet") var id_outlet: String?,
) : Parcelable
