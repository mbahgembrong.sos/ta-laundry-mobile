package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Layanan(
    @SerializedName("id") var id: String,
    @SerializedName("nama") var nama: String,
    @SerializedName("jenis_layanan") var jenisLayanan: String,
    @SerializedName("jenis_estimasi") var jenisEstimasi: String,
    @SerializedName("estimasi_selesai") var estimasiSelesai: Int,
    @SerializedName("satuan_hitung") var satuanHitung: String,
    @SerializedName("harga") var harga: Int,
    @SerializedName("proses") var proses: Int,
    @SerializedName("keterangan") var keterangan: String?,
    @SerializedName("foto") var foto: String?,
) : Parcelable
