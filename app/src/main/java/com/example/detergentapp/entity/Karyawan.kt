package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Karyawan(
    @SerializedName("id") var id: String,
    @SerializedName("outlet") var outlet: Outlet? = null,
//    @SerializedName("id_outlet") var idOutlet: String? = null,
    @SerializedName("id_level") var idLevel: String,
    @SerializedName("nama") var nama: String,
    @SerializedName("email") var email: String,
    @SerializedName("foto") var foto: String?,
    @SerializedName("alamat") var alamat: String,
    @SerializedName("telp") var telp: String,

    ) : Parcelable
