package com.example.detergentapp.entity

import com.google.gson.annotations.SerializedName

data class ResponseData<out T>(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val `data`: T?,
    @SerializedName("message")
    val message: String
)
