package com.example.detergentapp.entity

import com.google.gson.annotations.SerializedName

data class TransaksiRequest(
    @SerializedName("id_pelanggan") var idPelanggan: String,
    @SerializedName("id_promo") var idPromo: String?,
    @SerializedName("cart") var cart: List<Cart>,
    @SerializedName("outlet") var outlet: Outlet,
    @SerializedName("grand_total") var grandTotal: Int,
    @SerializedName("keterangan") var keterangan: String?,
)
