package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailTransaksiLayanan(
    @SerializedName("id") var id: String? = null,
    @SerializedName("id_transaksi") var idTransaksi: String,
    @SerializedName("layanan") var layanan: Layanan?,
    @SerializedName("satuan") var satuan: Int,
    @SerializedName("berat") var berat: Int?,
    @SerializedName("total_harga") var totalHarga: Int,
//    @SerializedName("updated_at") var updatedAt: String? = null,
//    @SerializedName("created_at") var createdAt: String? = null,
) : Parcelable
