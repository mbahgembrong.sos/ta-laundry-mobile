package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Outlet(
    @SerializedName("id") var id: String,
    @SerializedName("nama") var nama: String? = "",
    @SerializedName("foto") var foto: String? = "",
    @SerializedName("lokasi") var lokasi: String,
    @SerializedName("latitude") var latitude: Double,
    @SerializedName("longitude") var longitude: Double,
) : Parcelable
