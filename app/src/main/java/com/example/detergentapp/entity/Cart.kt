package com.example.detergentapp.entity

import com.google.gson.annotations.SerializedName

data class Cart(
    @SerializedName("layanan")
    var layanan: Layanan,
    @SerializedName("satuan")
    var satuan: Int,
    @SerializedName("total_harga")
    var totalHarga: Int,
)
