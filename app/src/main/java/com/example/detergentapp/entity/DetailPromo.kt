package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class DetailPromo(
    @SerializedName("id") var id: String,
    @SerializedName("id_promo") var idPromo: String,
    @SerializedName("layanan") var layanan: Layanan,
) : Parcelable
