package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailTracking(
    @SerializedName("id") var id: String? = null,
    @SerializedName("id_transaksi") var idTransaksi: String,
    @SerializedName("proses") var proses: String,
    @SerializedName("karyawan") var karyawan: Karyawan? = null,
    @SerializedName("updated_at") var updatedAt: String? = "",
    @SerializedName("created_at") var createdAt: String? = "",
) : Parcelable
