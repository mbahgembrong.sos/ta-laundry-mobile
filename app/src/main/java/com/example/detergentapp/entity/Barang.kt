package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Barang (
        @SerializedName("id") var id:String,
        @SerializedName("id_outlet") var idOutlet:String,
        @SerializedName("nama") var nama:String,
        @SerializedName("jumlah") var jumlah:Int,
): Parcelable
