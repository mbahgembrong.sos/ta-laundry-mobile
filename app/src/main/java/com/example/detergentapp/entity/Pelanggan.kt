package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Pelanggan(
    @SerializedName("id") var id: String?,
    @SerializedName("nama") var nama: String?,
    @SerializedName("email") var email: String?,
    @SerializedName("jenis_kelamin") var jenisKelamin: String?,
    @SerializedName("foto") var foto: String?,
    @SerializedName("telp") var telp: String?,
    @SerializedName("alamat") var alamat: String?,
    @SerializedName("password") var password: String?,
    @SerializedName("longitude") var longtitude: Double? = null,
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("token_fcm") var tokenFCM: String?,
    @SerializedName("file") var file: String? = ""
) : Parcelable
