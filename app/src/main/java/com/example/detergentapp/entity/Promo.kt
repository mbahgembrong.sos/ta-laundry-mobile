package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Promo(
    @SerializedName("id") var id: String,
    @SerializedName("nama") var nama: String,
    @SerializedName("deskripsi") var deskripsi: String?,
    @SerializedName("kode") var kode: String,
    @SerializedName("foto") var foto: String,
    @SerializedName("jenis_promo") var jenisPromo: String,
    @SerializedName("syarat_minimal") var syaratMinimal: Int?,
    @SerializedName("jenis_diskon") var jenisDiskon: String,
    @SerializedName("jumlah_diskon") var jumlahDiskon: Int,
    @SerializedName("tanggal_mulai") var tanggalMulai: String,
    @SerializedName("tanggal_selesai") var tanggalSelesai: String,
    @SerializedName("detail_promo") var detailPromo: List<DetailPromo>?,

    ) : Parcelable
