package com.example.detergentapp.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Transaksi(
    @SerializedName("id") var id: String?,
    @SerializedName("pelanggan") var pelanggan: Pelanggan,
    @SerializedName("promo") var promo: Promo?,
    @SerializedName("outlet") var outlet: Outlet? = null,
    @SerializedName("detail_transaksi_layanan") var detailTransaksiLayanan: List<DetailTransaksiLayanan?>? = null,
    @SerializedName("detail_tracking") var detailTracking: List<DetailTracking?>?,
    @SerializedName("satuan") var satuan: Int?,
    @SerializedName("jenis_transaksi") var jenisTransaksi: String?,
    @SerializedName("status_proses") var statusProses: String?,
    @SerializedName("status_pembayaran") var statusPembayaran: String?,
    @SerializedName("grand_total") var grandTotal: Int,
    @SerializedName("foto") var foto: String? = null,
    @SerializedName("keterangan") var keterangan: String?,
    @SerializedName("created_at") var createdAt: String?,
    @SerializedName("updated_at") var updateddAt: String?,
    @SerializedName("ulasan") var ulasan: Ulasan?,
    @SerializedName("buktiTransaksi") var buktiTransaksi: String? = null
) : Parcelable
