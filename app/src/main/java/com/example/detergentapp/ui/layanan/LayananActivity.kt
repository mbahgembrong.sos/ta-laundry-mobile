package com.example.detergentapp.ui.layanan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityLayananBinding
import com.example.detergentapp.databinding.ActivityMainBinding
import com.example.detergentapp.databinding.LayananBottomSheetBinding
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.entity.Transaksi
import com.example.detergentapp.ui.main.home.HomeViewModel
import com.example.detergentapp.ui.main.home.LayananGridAdapter
import com.example.detergentapp.ui.main.home.OrderListAdapter
import com.example.detergentapp.ui.transaction.TransactionActivity
import com.example.detergentapp.utils.AppConstants
import com.example.detergentapp.utils.CartHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso

class LayananActivity : AppCompatActivity() {
    lateinit var binding: ActivityLayananBinding
    lateinit var viewModel: LayananViewModel
    lateinit var layananAdapter: LayananGridAdapter
    var layananList = ArrayList<Layanan>()
    lateinit var bindingLayananBottomSheet: LayananBottomSheetBinding
    lateinit var layananSheetBehavior: BottomSheetBehavior<*>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun onResume() {
        super.onResume()
        observer()
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(LayananViewModel::class.java)
        viewModel.getLayananList.observe(this, Observer<List<Layanan>> {
            if (it == null) {
                Toast.makeText(this, "No result found", Toast.LENGTH_LONG).show()
            } else {
                layananList.clear()
                it.let { it1 -> layananList.addAll(it1) }
                layananAdapter.notifyDataSetChanged()
            }
        })
        layananAdapter.notifyDataSetChanged()
        viewModel.layananList()
    }

    private fun initView() {
        binding = ActivityLayananBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecycleView()
    }

    private fun setupRecycleView() {
        layananAdapter = LayananGridAdapter(this, layananList, object : LayananGridAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                layananBottomSheet(layananList[position]).show()
            }

            override fun onTambahClick(position: Int) {
                CartHelper.addItem(Cart(layananList[position], 0, 0))
                onBackPressed()
            }

        })
        binding.rvLayanan.layoutManager = GridLayoutManager(this, 2)
        binding.rvLayanan.adapter = layananAdapter
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun layananBottomSheet(layanan: Layanan): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingLayananBottomSheet = LayananBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingLayananBottomSheet.root)
        layananSheetBehavior = (dialog as BottomSheetDialog).behavior
        layananSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        layananSheetBehavior.isHideable = true
        if (!layanan.foto.isNullOrEmpty())
            Picasso.get().load("${AppConstants.URL_IMAGE_LAYANAN}${layanan.foto}").fit()
                .into(bindingLayananBottomSheet.imageLayanan)
        bindingLayananBottomSheet.textNamaLayanan.text = layanan.nama
        bindingLayananBottomSheet.textProses.text = "Proses: ${prosesLayanan(layanan.proses)}"
        bindingLayananBottomSheet.textJenisLayanan.text = layanan.jenisLayanan
        bindingLayananBottomSheet.textEstimasi.text = "${layanan.estimasiSelesai} ${layanan.jenisEstimasi}"
        bindingLayananBottomSheet.textHarga.text = "Rp. ${layanan.harga}/${layanan.satuanHitung}"
        if (!layanan.keterangan.isNullOrEmpty())
            bindingLayananBottomSheet.textKeterangan.text = layanan.keterangan
        bindingLayananBottomSheet.buttonTambah.setOnClickListener {
            CartHelper.addItem(Cart(layanan, 0, 0))
            dialog.dismiss()
            val intent = Intent(this, TransactionActivity::class.java)
            startActivity(intent).also { finish() }
        }
        return dialog
    }

    fun prosesLayanan(proses: Int): String {
        val prosesNama = arrayListOf<String>("Cuci", "Kering", "Setrika")
        return when (proses) {
            1 -> prosesNama[0]
            2 -> prosesNama[1]
            3 -> prosesNama[2]
            4 -> prosesNama.joinToString(", ")
            5 -> "${prosesNama[0]}, ${prosesNama[1]}"
            6 -> "${prosesNama[0]}, ${prosesNama[2]}"
            7 -> "${prosesNama[1]}, ${prosesNama[2]}"
            else -> "-"
        }
    }
}
