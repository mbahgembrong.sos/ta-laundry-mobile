package com.example.detergentapp.ui.main.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.R
import com.example.detergentapp.databinding.FragmentHomeBinding
import com.example.detergentapp.databinding.LayananBottomSheetBinding
import com.example.detergentapp.databinding.PromoDetailBottomSheetBinding
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.entity.Promo
import com.example.detergentapp.entity.Transaksi
import com.example.detergentapp.ui.layanan.LayananActivity
import com.example.detergentapp.ui.main.MainActivity
import com.example.detergentapp.ui.main.order.OrderDetailActivity
import com.example.detergentapp.ui.main.order.OrderDetailActivity.Companion.DETAIL_TRACKING
import com.example.detergentapp.ui.transaction.TransactionActivity
import com.example.detergentapp.utils.AppConstants
import com.example.detergentapp.utils.CartHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso
import timber.log.Timber

class HomeFragment : Fragment() {
    lateinit var viewModel: HomeViewModel
    lateinit var binding: FragmentHomeBinding
    lateinit var layananAdapter: LayananGridAdapter
    lateinit var orderAdapter: OrderListAdapter
    lateinit var promoAdapter: PromoListAdapter
    private var layananList: ArrayList<Layanan> = ArrayList()
    private var orderList: ArrayList<Transaksi> = ArrayList()
    private var promoList: ArrayList<Promo> = ArrayList()
    lateinit var bindingLayananBottomSheet: LayananBottomSheetBinding
    lateinit var layananSheetBehavior: BottomSheetBehavior<*>
    lateinit var bindingPromoBottomSheetBinding: PromoDetailBottomSheetBinding
    lateinit var promoSheetBehavior: BottomSheetBehavior<*>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        if (!prefs?.getCartItems().isNullOrEmpty())
            binding.buttonChart.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    val intent = Intent(requireContext(), TransactionActivity::class.java)
                    startActivity(intent).also { activity?.finish() }
                }
            }
        binding.buttonNotification.setOnClickListener {
//            val intent = Intent(requireContext(), NotificationActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
//            startActivity(intent).also { requireContext().finish() }
        }
        binding.buttonAllLayanan.setOnClickListener {
            val intent = Intent(requireContext(), LayananActivity::class.java)
            startActivity(intent)
        }
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewModel.getLayananList.observe(this, Observer<List<Layanan>> {
            if (it == null) {
                Toast.makeText(requireContext(), "Layanan tidak ditemukan", Toast.LENGTH_LONG).show()
            } else {
                layananList.clear()
                it.take(5).let { it1 -> layananList.addAll(it1) }
                layananAdapter.notifyDataSetChanged()
            }
        })
        viewModel.getOrderList.observe(this) {
            if (it == null || it == listOf<Transaksi>()) {
                binding.layoutOrder.visibility = View.GONE
                Toast.makeText(requireContext(), "Tidak ada order yang aktif", Toast.LENGTH_LONG).show()
            } else {
                binding.layoutOrder.visibility = View.VISIBLE
                orderList.clear()
                orderList.addAll(it)
                orderAdapter.notifyDataSetChanged()
            }
        }
        viewModel.getPromoList.observe(this) {
            if (it == null) {
                Toast.makeText(requireContext(), "Tidak tersedia promo", Toast.LENGTH_LONG).show()
            } else {
                promoList.clear()
                it.let { it1 -> promoList.addAll(it1) }
                promoAdapter.notifyDataSetChanged()
            }
        }
        viewModel.let {
            it.layananList()
            it.promoList()
            it.orderList(prefs?.id!!)
        }
    }

    private fun initView() {
        binding.textGretting.text = prefs?.nama ?: "Detergent Laundry"
        setupRecycleView()
    }

    private fun setupRecycleView() {
        layananAdapter =
            LayananGridAdapter(requireContext(), layananList, object : LayananGridAdapter.OnItemClickListener {
                override fun onItemClick(position: Int) {
                    layananBottomSheet(layananList[position]).show()
                }

                override fun onTambahClick(position: Int) {
                    CartHelper.addItem(Cart(layananList[position], 0, 0))
                    val intent = Intent(requireContext(), TransactionActivity::class.java)
                    startActivity(intent).also { activity?.finish() }
                }
            })
        binding.rvLayanan.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false)
        binding.rvLayanan.adapter = layananAdapter
        orderAdapter = OrderListAdapter(requireContext(), orderList, object : OrderListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {

            }

            override fun onTrackOrder(position: Int) {
                val intent = Intent(requireContext(), OrderDetailActivity::class.java)
                intent.putExtra(DETAIL_TRACKING, orderList[position])
                startActivity(intent)
            }
        })
        binding.rvOrder.layoutManager = LinearLayoutManager(activity)
        binding.rvOrder.adapter = orderAdapter
        promoAdapter = PromoListAdapter(requireContext(), promoList, object : PromoListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                promoDetailBottomSheet(promoList[position]).show()
            }
        })
        binding.rvPromo.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false)
        binding.rvPromo.adapter = promoAdapter
    }

    fun layananBottomSheet(layanan: Layanan): BottomSheetDialog {
        val dialog = BottomSheetDialog(requireContext())
        bindingLayananBottomSheet = LayananBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingLayananBottomSheet.root)
        layananSheetBehavior = (dialog as BottomSheetDialog).behavior
        layananSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        layananSheetBehavior.isHideable = true
        if (!layanan.foto.isNullOrEmpty())
            Picasso.get().load("${AppConstants.URL_IMAGE_LAYANAN}${layanan.foto}").fit()
                .into(bindingLayananBottomSheet.imageLayanan)
        bindingLayananBottomSheet.textNamaLayanan.text = layanan.nama
        bindingLayananBottomSheet.textProses.text = "Proses: ${prosesLayanan(layanan.proses)}"
        bindingLayananBottomSheet.textJenisLayanan.text = layanan.jenisLayanan
        bindingLayananBottomSheet.textEstimasi.text = "${layanan.estimasiSelesai} ${layanan.jenisEstimasi}"
        bindingLayananBottomSheet.textHarga.text = "Rp. ${layanan.harga}/${layanan.satuanHitung}"
        if (!layanan.keterangan.isNullOrEmpty())
            bindingLayananBottomSheet.textKeterangan.text = layanan.keterangan
        bindingLayananBottomSheet.buttonTambah.setOnClickListener {
            CartHelper.addItem(Cart(layanan, 0, 0))
            dialog.dismiss()
            val intent = Intent(requireContext(), TransactionActivity::class.java)
            startActivity(intent).also { activity?.finish() }
        }
        return dialog
    }

    fun prosesLayanan(proses: Int): String {
        val prosesNama = arrayListOf<String>("Cuci", "Kering", "Setrika")
        return when (proses) {
            1 -> prosesNama[0]
            2 -> prosesNama[1]
            3 -> prosesNama[2]
            4 -> prosesNama.joinToString(", ")
            5 -> "${prosesNama[0]}, ${prosesNama[1]}"
            6 -> "${prosesNama[0]}, ${prosesNama[2]}"
            7 -> "${prosesNama[1]}, ${prosesNama[2]}"
            else -> "-"
        }
    }

    fun promoDetailBottomSheet(promo: Promo): BottomSheetDialog {
        val dialog = BottomSheetDialog(requireContext())
        bindingPromoBottomSheetBinding = PromoDetailBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingPromoBottomSheetBinding.root)
        promoSheetBehavior = (dialog as BottomSheetDialog).behavior
        promoSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        promoSheetBehavior.isHideable = true
        if (!promo.foto.isNullOrEmpty())
            Picasso.get().load("${AppConstants.URL_IMAGE_PROMO}${promo.foto}").fit().placeholder(R.drawable.ic_orders)
                .error(R.drawable.ic_orders)
                .into(bindingPromoBottomSheetBinding.imageLayanan)
        bindingPromoBottomSheetBinding.textNamaPromo.text = promo.nama
        bindingPromoBottomSheetBinding.textDatePromo.text = "${promo.tanggalMulai} - ${promo.tanggalSelesai}"
        if (promo.jenisPromo != "tanpa") {
            bindingPromoBottomSheetBinding.textSyaratMinimalPromo.text =
                "Minimal transaksi }" + if (promo.jenisPromo == "quantity") "${promo.syaratMinimal} items" else "Rp. ${promo.syaratMinimal}"
        } else {
            bindingPromoBottomSheetBinding.textSyaratMinimalPromo.text = "Tanpa syarat"
        }
        bindingPromoBottomSheetBinding.textJumlahDiskonPromo.text =
            "Diskon " + if (promo.jenisDiskon == "nominal") "Rp. ${promo.jumlahDiskon}" else "${promo.jumlahDiskon} %"
        if (!promo.deskripsi.isNullOrEmpty())
            bindingPromoBottomSheetBinding.textKeteranganPromo.text = promo.deskripsi
        val layananPromoAdapter = LayananPromoAdapter(requireContext(), promo.detailPromo!!)
        bindingPromoBottomSheetBinding.rvLayananPromo.layoutManager =
            GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false)
        bindingPromoBottomSheetBinding.rvLayananPromo.adapter = layananPromoAdapter
        return dialog
    }
}
