package com.example.detergentapp.ui.auth

import android.media.session.MediaSession.Token
import android.provider.Settings
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.App
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.entity.ResponseData
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.PelangganService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class AuthViewModel : ViewModel() {
    private val pelanggan = MutableLiveData<Pelanggan?>()
    val getPelanggan: LiveData<Pelanggan?>
        get() = pelanggan


    fun login(email: String, password: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(PelangganService::class.java)
        val call = retroInstance.postPelangganLogin(email, password)
        call.enqueue(object : Callback<ResponseData<Pelanggan>?> {
            override fun onResponse(
                call: Call<ResponseData<Pelanggan>?>,
                response: Response<ResponseData<Pelanggan>?>
            ) {
                Timber.e("message: $response")
                Timber.e("message: $call")
                if (response.isSuccessful) {
                    pelanggan.value = response.body()?.data
                    saveTokenFcm(pelanggan.value?.id!!, App.tokenFCM!!)
                } else {
                    pelanggan.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Pelanggan>?>, t: Throwable) {
                Timber.e("message: $t")
                Timber.e("message: $call")
                pelanggan.value = null
            }

        })
    }

    fun register(user: Pelanggan) {
        val retroInstance = ApiConfig.getRetroInstance().create(PelangganService::class.java)
        val call = retroInstance.postPelangganRegister(
            user.nama!!,
            user.email!!,
            user.jenisKelamin!!,
            user.telp!!,
            user.alamat!!,
            user.password!!,
            user.foto!!,
            user.longtitude!!,
            user.latitude!!,
            user.tokenFCM!!,
            user.file
        )
        call.enqueue(object : Callback<ResponseData<Pelanggan>?> {
            override fun onResponse(
                call: Call<ResponseData<Pelanggan>?>,
                response: Response<ResponseData<Pelanggan>?>
            ) {
                Timber.d("response : $response")
                if (response.isSuccessful) {
                    pelanggan.value = response.body()?.data
                    saveTokenFcm(pelanggan.value?.id!!, App.tokenFCM!!)
                } else {
                    pelanggan.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Pelanggan>?>, t: Throwable) {
                pelanggan.value = null
                Timber.e("error : $t  $call")
            }

        })
    }

    fun saveTokenFcm(pelangganId: String, token: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(PelangganService::class.java)
        val call = retroInstance.postPelangganTokenFcm(token, pelangganId)
        call.enqueue(object : Callback<ResponseData<Pelanggan>?> {
            override fun onResponse(
                call: Call<ResponseData<Pelanggan>?>,
                response: Response<ResponseData<Pelanggan>?>
            ) {
                Timber.d("response : $response")
                if (response.isSuccessful) {
                    pelanggan.value = response.body()?.data
                } else {
                    pelanggan.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Pelanggan>?>, t: Throwable) {
                pelanggan.value = null
                Timber.e("error : $t  $call")
            }

        })
    }

}
