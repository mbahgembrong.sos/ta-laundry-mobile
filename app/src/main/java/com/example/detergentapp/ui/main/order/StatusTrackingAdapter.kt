package com.example.detergentapp.ui.main.order

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.databinding.ItemOrderStatusBinding
import com.example.detergentapp.entity.DetailTracking
import com.example.detergentapp.entity.Layanan
import timber.log.Timber


class StatusTrackingAdapter(
    private val context: Context,
    private val trackingItemList: List<DetailTracking>
) : RecyclerView.Adapter<StatusTrackingAdapter.StatusItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StatusItemViewHolder {
        val binding: ItemOrderStatusBinding =
            ItemOrderStatusBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StatusItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StatusItemViewHolder, position: Int) {
        holder.bind(trackingItemList[position], holder.adapterPosition)
    }

    override fun getItemCount(): Int {
        return trackingItemList.size
    }

    class StatusItemViewHolder(var binding: ItemOrderStatusBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(tracking: DetailTracking, position: Int) {
            binding.textStatus.text = tracking.proses
            binding.textTime.text = tracking.createdAt
            if (tracking.karyawan != null)
                binding.textKaryawan.apply {
                    visibility = ViewGroup.VISIBLE
                    text = "${tracking.karyawan!!.outlet?.nama ?: "-"} - ${tracking.karyawan!!.nama} "
                }
        }
    }

}
