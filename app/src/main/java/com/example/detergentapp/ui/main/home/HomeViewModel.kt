package com.example.detergentapp.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.LayananService
import com.example.detergentapp.data.retrofit.service.PromoService
import com.example.detergentapp.data.retrofit.service.TransaksiService
import com.example.detergentapp.entity.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class HomeViewModel : ViewModel() {
    private val layananList = MutableLiveData<List<Layanan>>()
    val getLayananList: LiveData<List<Layanan>>
        get() = layananList

    private val orderList = MutableLiveData<List<Transaksi>>()
    val getOrderList: LiveData<List<Transaksi>>
        get() = orderList

    private val promoList = MutableLiveData<List<Promo>>()
    val getPromoList: LiveData<List<Promo>>
        get() = promoList

    fun layananList() {
        val retroInstance = ApiConfig.getRetroInstance().create(LayananService::class.java)
        val call = retroInstance.getLayananList()
        call.enqueue(object : Callback<ResponseData<List<Layanan>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Layanan>>?>,
                response: Response<ResponseData<List<Layanan>>?>
            ) {
                if (response.isSuccessful) {

                    layananList.value = response.body()?.data
                } else {
                    layananList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Layanan>>?>, t: Throwable) {
                layananList.value = null
            }

        })
    }

    fun orderList(idPelanggan: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.getTransaksiList(idPelanggan)
        call.enqueue(object : Callback<ResponseData<List<Transaksi>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Transaksi>>?>,
                response: Response<ResponseData<List<Transaksi>>?>
            ) {
                Timber.d("response: ${response.body()?.data}")
                if (response.isSuccessful) {
                    orderList.value =
                        response.body()?.data?.filter { it.statusProses != "selesai" && it.statusProses != "cancel" }
                            ?.sortedByDescending { it.createdAt }
                } else {
                    orderList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Transaksi>>?>, t: Throwable) {
                Timber.e("error: ${t.message}")
                orderList.value = null
            }

        })
    }

    fun promoList() {
        val retroInstance = ApiConfig.getRetroInstance().create(PromoService::class.java)
        val call = retroInstance.getPromoList()
        call.enqueue(object : Callback<ResponseData<List<Promo>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Promo>>?>,
                response: Response<ResponseData<List<Promo>>?>
            ) {
                if (response.isSuccessful) {
                    promoList.value = response.body()?.data
                } else {
                    promoList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Promo>>?>, t: Throwable) {
                promoList.value = null
            }
        })
    }
}
