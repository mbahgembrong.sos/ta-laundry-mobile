package com.example.detergentapp.ui.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.OutletService
import com.example.detergentapp.data.retrofit.service.PromoService
import com.example.detergentapp.data.retrofit.service.TransaksiService
import com.example.detergentapp.entity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class TransactionViewModel : ViewModel() {
    private val transaksi = MutableLiveData<Transaksi>()
    val getTransaksi: LiveData<Transaksi>
        get() = transaksi
    private val isBayar = MutableLiveData<Boolean>()
    val getBayar: LiveData<Boolean>
        get() = isBayar
    private val cartList = MutableLiveData<List<Cart?>?>()
    val getCartList: LiveData<List<Cart?>?>
        get() = cartList
    private val outletList = MutableLiveData<List<Outlet?>?>()
    val getOutletList: LiveData<List<Outlet?>?>
        get() = outletList
    private val promoList = MutableLiveData<List<Promo?>?>()
    val getPromoList: LiveData<List<Promo?>?>
        get() = promoList

    fun addTransakis(outlet: Outlet, cartItems: List<Cart>, grandTotal: Int, promo: String?, keterangan: String?) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.postTambahTransaksi(
            TransaksiRequest(
                prefs?.id!!,
                promo,
                cartItems,
                outlet,
                grandTotal,
                keterangan
            )
        )
        call.enqueue(object : Callback<ResponseData<Transaksi>?> {
            override fun onResponse(
                call: Call<ResponseData<Transaksi>?>,
                response: Response<ResponseData<Transaksi>?>
            ) {
                Timber.d("response : $response")
                if (response.isSuccessful && response.body()?.status == true) {
                    transaksi.value = response.body()?.data
                } else {
                    transaksi.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Transaksi>?>, t: Throwable) {
                transaksi.value = null
                Timber.e("error : $t  $call")
            }

        })
    }

    fun bayarTransaksi(id: String, foto: String, file: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.postBayarTransaksi(
            id,
            foto,
            file
        )
        call.enqueue(object : Callback<ResponseData<Transaksi>?> {
            override fun onResponse(
                call: Call<ResponseData<Transaksi>?>,
                response: Response<ResponseData<Transaksi>?>
            ) {
                if (response.isSuccessful) {
                    if (response.body()?.status == true) {
                        Timber.d("response : $response")
                        isBayar.value == true
                    } else
                        isBayar.value == false
                } else {
                    isBayar.value = false
                }
            }

            override fun onFailure(call: Call<ResponseData<Transaksi>?>, t: Throwable) {
                isBayar.value = false
                Timber.e("error : $t  $call")
            }

        })
    }

    fun getCart() {
        cartList.value = if (prefs?.getCartItems().isNullOrEmpty())
            mutableListOf()
        else
            prefs?.getCartItems()
    }

    fun addQuantity(cartItem: Cart) {
        var cart: MutableList<Cart> = if (prefs?.getCartItems().isNullOrEmpty())
            mutableListOf()
        else
            prefs?.getCartItems() as MutableList<Cart>
        val targetItem = cart.singleOrNull { it.layanan.id == cartItem.layanan.id }
        if (targetItem == null) {
            cartItem.satuan++
            cartItem.totalHarga = cartItem.satuan * cartItem.layanan.harga
            cart.add(cartItem)
        } else {
            targetItem.satuan++
            targetItem.totalHarga = targetItem.satuan * targetItem.layanan.harga
        }
        prefs?.saveCart(cart)
        getCart()
    }

    fun subQuantity(cartItem: Cart) {
        val cart = prefs?.getCartItems() as MutableList<Cart>
        val targetItem = cart.singleOrNull { it.layanan.id == cartItem.layanan.id }
        if (targetItem != null) {
            if (targetItem.satuan > 1) {
                targetItem.satuan--
                targetItem.totalHarga = targetItem.satuan * targetItem.layanan.harga
            } else {
                cart.remove(targetItem)
            }
        }
        prefs!!.saveCart(cart)
        getCart()
    }

    fun getOutlet(search: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(OutletService::class.java)
        val call = retroInstance.getOutletList(search)
        call.enqueue(object : Callback<ResponseData<List<Outlet>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Outlet>>?>,
                response: Response<ResponseData<List<Outlet>>?>
            ) {
                if (response.isSuccessful) {
                    outletList.value = response.body()?.data
                } else {
                    outletList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Outlet>>?>, t: Throwable) {
                outletList.value = null
            }

        })
    }

    fun getPromo(promo: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(PromoService::class.java)
        val call = retroInstance.getPromoList(promo)
        call.enqueue(object : Callback<ResponseData<List<Promo>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Promo>>?>,
                response: Response<ResponseData<List<Promo>>?>
            ) {
                if (response.isSuccessful) {
                    promoList.value = response.body()?.data
                } else {
                    promoList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Promo>>?>, t: Throwable) {
                promoList.value = null
            }

        })
    }


}
