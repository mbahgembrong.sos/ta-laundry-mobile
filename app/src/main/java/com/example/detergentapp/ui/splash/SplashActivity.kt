package com.example.detergentapp.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.detergentapp.App
import com.example.detergentapp.databinding.ActivitySplashBinding
import com.example.detergentapp.ui.auth.LoginActivity
import com.example.detergentapp.ui.main.MainActivity
import com.google.firebase.messaging.FirebaseMessaging
import mumayank.com.airlocationlibrary.AirLocation
import timber.log.Timber

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        if (App.prefs?.id != null) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { finish() }
        }
        binding.btnGetStarted.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    fun initView() {
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}
