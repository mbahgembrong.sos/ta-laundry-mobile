package com.example.detergentapp.ui.main.order

import android.R
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.databinding.FragmentOrderBinding
import com.example.detergentapp.entity.Transaksi
import com.example.detergentapp.ui.main.home.OrderListAdapter
import timber.log.Timber
import java.util.StringJoiner

class OrderFragment : Fragment() {
    lateinit var binding: FragmentOrderBinding
    lateinit var orderAdapter: OrderListAdapter
    lateinit var viewModel: OrderViewModel
    private var orderList = ArrayList<Transaksi>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOrderBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
    }

    private fun outlet(orderList: ArrayList<Transaksi>) {
        val listOutlet = orderList.map { it.outlet?.id }.toMutableList().toMutableSet()
        var temp = mutableMapOf<String?, String?>(
            "all" to "Semua Outlet",
        )
        listOutlet.map {
            val outlet = orderList.find { it1 -> it1.outlet?.id == it }?.outlet
            hashMapOf(
                if (outlet?.id.toString() != "null") outlet?.id.toString() to outlet?.nama.toString() else outlet?.id to "No Outlet"
            )

        }.forEach {
            temp.putAll(it)
        }

        Timber.d("outlet ${temp}")
        val adapterOutlet = ArrayAdapter(
            requireContext(),
            R.layout.simple_spinner_dropdown_item,
            temp.values.toMutableList()
        )
        binding.spOutlet.adapter = adapterOutlet

        binding.spOutlet.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                // your code here
                Timber.d("outlet ${temp.keys.toList()[position]}")
                if (temp.keys.toList()[position] != "all") {
                    orderAdapter.orderItemList = orderList.filter { it.outlet?.id == temp.keys.toList()[position] }
                    orderAdapter.notifyDataSetChanged()
                } else {
                    orderAdapter.orderItemList = orderList
                    orderAdapter.notifyDataSetChanged()
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })
    }

    private fun statusOrder(orderList: ArrayList<Transaksi>) {
        val listStatus = orderList.map { it.statusProses }.toMutableList().toMutableSet()
        var temp = mutableMapOf<String?, String?>(
            "all" to "Semua Status",
        )
        listStatus.map {
            hashMapOf(
                it to it
            )

        }.forEach {
            temp.putAll(it)
        }

        Timber.d("status ${temp}")
        val adapterStatus = ArrayAdapter(
            requireContext(),
            R.layout.simple_spinner_dropdown_item,
            temp.values.toMutableList()
        )
        binding.spTracking.adapter = adapterStatus

        binding.spTracking.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                // your code here
                Timber.d("status ${temp.keys.toList()[position]}")
                if (temp.keys.toList()[position] != "all") {
                    orderAdapter.orderItemList = orderList.filter { it.statusProses == temp.keys.toList()[position] }
                    orderAdapter.notifyDataSetChanged()
                } else {
                    orderAdapter.orderItemList = orderList
                    orderAdapter.notifyDataSetChanged()
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(OrderViewModel::class.java)
        viewModel.getOrderList.observe(viewLifecycleOwner) {
            if (it == null) {
                Toast.makeText(requireContext(), "No result found", Toast.LENGTH_LONG).show()
            } else {
                orderList.clear()
                it.let { it1 -> orderList.addAll(it1) }
                orderAdapter.notifyDataSetChanged()
                outlet(orderList)
                statusOrder(orderList)
            }
        }
        viewModel.orderList(prefs?.id!!)
    }

    private fun initView() {
        setupRecycleView()
    }

    private fun setupRecycleView() {
        orderAdapter = OrderListAdapter(requireContext(), orderList, object : OrderListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {

            }

            override fun onTrackOrder(position: Int) {
                val intent = Intent(requireContext(), OrderDetailActivity::class.java)
                intent.putExtra(OrderDetailActivity.DETAIL_TRACKING, orderList[position])
                startActivity(intent)
            }

        })
        binding.rvOrder.layoutManager = LinearLayoutManager(requireContext())
        binding.rvOrder.adapter = orderAdapter
    }


}
