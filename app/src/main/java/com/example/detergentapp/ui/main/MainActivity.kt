package com.example.detergentapp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityMainBinding
import com.example.detergentapp.ui.main.home.HomeFragment
import com.example.detergentapp.ui.main.order.OrderFragment
import com.example.detergentapp.ui.main.outlet.OutletFragment
import com.example.detergentapp.ui.main.profile.ProfileFragment
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        navigateFragment(HomeFragment())
        binding.navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_menu_home -> {
                    navigateFragment(HomeFragment())
                    Timber.i("Home")
                    true
                }
                R.id.nav_menu_outlet -> {
                    navigateFragment(OutletFragment())
                    Timber.i("Dashboard")
                    true
                }
                R.id.nav_menu_order -> {
                    navigateFragment(OrderFragment())
                    Timber.i("Notifications")
                    true
                }
                R.id.nav_menu_profile -> {
                    navigateFragment(ProfileFragment())
                    Timber.i("Profile")
                    true
                }
                else -> false
            }
        }
    }

    fun initView() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun navigateFragment(fragment: androidx.fragment.app.Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
