package com.example.detergentapp.ui.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.PelangganService
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.entity.ResponseData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class ProfileViewModel : ViewModel() {
    private val pelanggan = MutableLiveData<Pelanggan?>()
    val getPelanggan: LiveData<Pelanggan?>
        get() = pelanggan

    fun update(user: Pelanggan) {
        val retroInstance = ApiConfig.getRetroInstance().create(PelangganService::class.java)
        val call = retroInstance.postPelangganUpdate(
            user.id!!,
            user.nama!!,
            user.email!!,
            user.jenisKelamin!!,
            user.telp!!,
            user.alamat!!,
            user.password,
            user.foto,
            user.longtitude,
            user.latitude,
            user.tokenFCM,
            user.file
        )
        call.enqueue(object : Callback<ResponseData<Pelanggan>?> {
            override fun onResponse(
                call: Call<ResponseData<Pelanggan>?>,
                response: Response<ResponseData<Pelanggan>?>
            ) {
                if (response.isSuccessful) {
                    pelanggan.value = response.body()?.data

                } else {
                    pelanggan.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Pelanggan>?>, t: Throwable) {
                pelanggan.value = null
            }

        })
    }

}
