package com.example.detergentapp.ui.main.outlet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.OutletService
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.entity.ResponseData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OutletViewModel:ViewModel() {
    private val outletList = MutableLiveData<List<Outlet>>()
    val getOutletList: LiveData<List<Outlet>?>
        get() = outletList
    fun outletList() {
        val retroInstance = ApiConfig.getRetroInstance().create(OutletService::class.java)
        val call = retroInstance.getOutletList()
        call.enqueue(object : Callback<ResponseData<List<Outlet>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Outlet>>?>,
                response: Response<ResponseData<List<Outlet>>?>
            ) {
                if(response.isSuccessful){
                    outletList.value = response.body()?.data
                }else{
                    outletList.value = null
                }
            }
            override fun onFailure(call: Call<ResponseData<List<Outlet>>?>, t: Throwable) {
                outletList.value = null
            }

        })
    }
}
