package com.example.detergentapp.ui.layanan

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.LayananService
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.entity.Promo
import com.example.detergentapp.entity.ResponseData
import com.example.detergentapp.entity.Transaksi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LayananViewModel : ViewModel() {
    private val layananList = MutableLiveData<List<Layanan>>()
    val getLayananList: LiveData<List<Layanan>>
        get() = layananList

    fun layananList() {
        val retroInstance = ApiConfig.getRetroInstance().create(LayananService::class.java)
        val call = retroInstance.getLayananList()
        call.enqueue(object : Callback<ResponseData<List<Layanan>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Layanan>>?>,
                response: Response<ResponseData<List<Layanan>>?>
            ) {
                if (response.isSuccessful) {
                    layananList.value = response.body()?.data
                } else {
                    layananList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Layanan>>?>, t: Throwable) {
                layananList.value = null
            }

        })
    }
}
