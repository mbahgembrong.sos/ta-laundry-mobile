package com.example.detergentapp.ui.main.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ItemPromoHomeBinding
import com.example.detergentapp.entity.Promo
import com.example.detergentapp.utils.AppConstants
import com.squareup.picasso.Picasso

class PromoListAdapter(
    private val context: Context,
    private val promoItemList: List<Promo>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<PromoListAdapter.PromoItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PromoItemViewHolder {
        val binding: ItemPromoHomeBinding =
            ItemPromoHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PromoItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PromoItemViewHolder, position: Int) {
        holder.bind(promoItemList[position], holder.adapterPosition, listener as OnItemClickListener)
    }

    override fun getItemCount(): Int {
        return promoItemList.size
    }

    class PromoItemViewHolder(var binding: ItemPromoHomeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(promo: Promo, position: Int, listener: OnItemClickListener) {
            Picasso.get().load("${AppConstants.URL_IMAGE_PROMO}${promo.foto}").fit()
                .placeholder(R.drawable.ic_orders)
                .error(R.drawable.ic_orders)
                .into(binding.imagePromoList)
            binding.itemRecommendedName.text = promo.nama
            binding.root.setOnClickListener { listener.onItemClick(position) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}
