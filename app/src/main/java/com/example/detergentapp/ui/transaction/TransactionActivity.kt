package com.example.detergentapp.ui.transaction

import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityTransactionBinding
import com.example.detergentapp.databinding.BayarBottomSheetBinding
import com.example.detergentapp.databinding.CameraBottomSheetBinding
import com.example.detergentapp.databinding.OutletBotomSheetBinding
import com.example.detergentapp.databinding.PromoBotomSheetBinding
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.entity.Promo
import com.example.detergentapp.ui.layanan.LayananActivity
import com.example.detergentapp.ui.main.MainActivity
import com.example.detergentapp.ui.main.home.PromoListAdapter
import com.example.detergentapp.ui.main.outlet.OutletListAdapter
import com.example.detergentapp.utils.CartHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.common.util.concurrent.ListenableFuture
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class TransactionActivity : AppCompatActivity() {
    lateinit var binding: ActivityTransactionBinding
    lateinit var viewModel: TransactionViewModel
    lateinit var cartLayananAdapter: CartLayananAdapter
    lateinit var bindingOutletBottomSheet: OutletBotomSheetBinding
    lateinit var outletAdapter: OutletListAdapter
    lateinit var bindingPromoBottomSheet: PromoBotomSheetBinding
    lateinit var promoAdapter: PromoListAdapter
    var grandTotal = 0
    var totalHarga = 0
    val biayaOngkir = 0
    var diskonHarga = 0
    var cartLayananList = ArrayList<Cart>()
    var outletList = ArrayList<Outlet>()
    var promoList = ArrayList<Promo>()
    var promoChecked: Promo? = null
    private lateinit var outletSheetBehavior: BottomSheetBehavior<*>
    lateinit var promoSheetBehavior: BottomSheetBehavior<*>
    lateinit var bindingBayarBottomSheetBinding: BayarBottomSheetBinding
    lateinit var bayarSheetBehavior: BottomSheetBehavior<*>
    lateinit var image: String
    lateinit var imageFile: String
    var fileUri = Uri.parse("")
    lateinit var transaksiId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (prefs?.getCartItems().isNullOrEmpty()) {
            val intent = Intent(this, LayananActivity::class.java)
            startActivity(intent)
        }
        initView()
        observer()
        binding.buttonTambahLaundry.setOnClickListener {
            val intent = Intent(this, LayananActivity::class.java)
            startActivity(intent)
        }


        binding.buttonPromo.setOnClickListener {
            promoBottomSheet().show()
        }


        binding.buttonGantiOutlet.setOnClickListener {
            outletBottomSheet().show()
        }

    }

    override fun onResume() {
        super.onResume()
        if (!prefs?.outlet.isNullOrEmpty()) {
            val outlet = prefs?.getOutletCart()
            binding.textNamaOutletTransaksi.text = outlet!!.nama
            binding.textAlamatOutletTransaksi.text = outlet!!.lokasi
            buttonPesanStyle()
        }
        viewModel.getCart()
        Timber.d("outlet cart: ${prefs?.getOutletCart()}")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent).also { finish() }
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)
        viewModel.getTransaksi.observe(this) {
            Timber.d("getTransaksi: $it")
            if (it == null) {
                Toast.makeText(this, "Gagal Tambah Transaksi", Toast.LENGTH_LONG).show()
            } else {
                transaksiId = it.id.toString()
                prefs?.clearCartPreferences()
                Toast.makeText(this, "Sukses Pesan Laundry, Segera bayar Laundrymu", Toast.LENGTH_LONG)
                bayarBottomSheet().show()
            }
        }
        viewModel.getCartList.observe(this) {
            if (it == null) {
                Toast.makeText(this, "Tidak ada data cart", Toast.LENGTH_LONG).show()
            } else {
                cartLayananList.clear()
                cartLayananList.addAll(it as ArrayList<Cart>) // add all items in the list
                cartLayananAdapter.notifyDataSetChanged()
                calculate()
            }
        }
        viewModel.getOutletList.observe(this) {
            if (it == null) {
                outletList.clear()
                outletAdapter.notifyDataSetChanged()
                Toast.makeText(this, "Tidak ada data outlet", Toast.LENGTH_LONG).show()
            } else {
                outletList.clear()
                outletList.addAll(it as ArrayList<Outlet>) // add all items in the list
                outletAdapter.notifyDataSetChanged()
            }
        }
        viewModel.getOutlet("")
        viewModel.getPromoList.observe(this) {
            if (it == null) {
                Toast.makeText(this, "Tidak ada data promo", Toast.LENGTH_LONG).show()
            } else {
                promoList.clear()
                promoList.addAll(it as ArrayList<Promo>) // add all items in the list
                promoAdapter.notifyDataSetChanged()
            }
        }
        viewModel.getPromo("")
        viewModel.getBayar.observe(this) {
            if (it == true) {
                Toast.makeText(this, "Sukses Pembayaran Laundry", Toast.LENGTH_SHORT)
                bayarBottomSheet().dismiss()
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent).also { finish() }
            } else {
                Toast.makeText(this, "Gagal Pembayaran Laundry", Toast.LENGTH_LONG)
                bindingBayarBottomSheetBinding.apply {
                    previewCamera.visibility = View.VISIBLE
                    imageBuktiPembayaran.visibility = View.INVISIBLE
                    buttonBayar.visibility = View.GONE
                    buttonTake.setText("AMBIL GAMBAR")
                }
            }
        }
    }

    private fun initView() {
        binding = ActivityTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupCartLayananRecycleView()
    }

    private fun setupCartLayananRecycleView() {
        cartLayananAdapter = CartLayananAdapter(this, cartLayananList, object : CartLayananAdapter.OnItemClickListener {
            override fun onItemClick(item: Layanan?, position: Int) {
            }

            override fun onQuantityAdd(position: Int) {
                viewModel.addQuantity(cartLayananList[position])
            }

            override fun onQuantitySub(position: Int) {
                viewModel.subQuantity(cartLayananList[position])
            }
        })
        binding.rvCartLayanan.layoutManager = LinearLayoutManager(this)
        binding.rvCartLayanan.adapter = cartLayananAdapter
        outletAdapter = OutletListAdapter(baseContext, outletList, object : OutletListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                prefs?.saveOutlet(outletList[position])
                binding.textNamaOutletTransaksi.text = outletList[position].nama
                binding.textAlamatOutletTransaksi.text = outletList[position].lokasi
                buttonPesanStyle()
                outletSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                outletBottomSheet().dismiss()
            }

            override fun onOrderClick(position: Int) {
                prefs?.saveOutlet(outletList[position])
                binding.textNamaOutletTransaksi.text = outletList[position].nama
                binding.textAlamatOutletTransaksi.text = outletList[position].lokasi
                buttonPesanStyle()
                outletSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                outletBottomSheet().dismiss()
            }

        })
        promoAdapter = PromoListAdapter(baseContext, promoList, object : PromoListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
//                prefs?.savePromo(promoList[position])
                promoChecked = promoList[position]

                promoSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                promoBottomSheet().dismiss()
                calculate()
            }
        })
    }


    fun calculate() {
        diskonHarga = 0
        cartLayananList.forEach {
            diskonHarga += cekPromo(promoChecked, it)
        }
        if (diskonHarga > 0) {
            binding.layoutDiskon.visibility = View.VISIBLE
            if (promoChecked != null) {
                binding.apply {
                    cardDiskon.visibility = View.VISIBLE
                    cardDiskon.background = ContextCompat.getDrawable(baseContext, R.color.colorAccent)
                    textPromoLabel.visibility = View.GONE
                    namaPromoSelected.text = promoChecked!!.nama
                    textSyaratMinimal.text =
                        "Minimal " + if (promoChecked!!.jenisPromo == "quantity") promoChecked!!.syaratMinimal else "Rp. ${promoChecked!!.syaratMinimal}"
                    textJumlahDiskon.text =
                        if (promoChecked!!.jenisDiskon == "nominal") "Rp. ${promoChecked!!.jumlahDiskon}" else "${promoChecked!!.jumlahDiskon}%"
                }
            } else {
                binding.apply {
                    cardDiskon.visibility = View.VISIBLE
                    textPromoLabel.visibility = View.GONE
                    cardDiskon.background = ContextCompat.getDrawable(baseContext, R.color.colorPrimaryDark)
                }
            }
        } else {
            binding.layoutDiskon.visibility = View.GONE
            binding.apply {
                cardDiskon.visibility = View.GONE
                textPromoLabel.visibility = View.VISIBLE
            }
        }
        totalHarga = CartHelper.getShoppingTotal()
        grandTotal = (totalHarga + biayaOngkir - diskonHarga)
        binding.textTotalHarga.text = "Rp. $totalHarga"
        binding.textBiayaOngkir.text = "Rp. $biayaOngkir"
        binding.textDiskonPromo.text = "Rp. -$diskonHarga"
        binding.textGrandTotal.text = "Rp. $grandTotal"
        buttonPesanStyle()
    }

    fun buttonPesanStyle() {
        if (prefs?.getOutletCart() == null) {
            Timber.d("buttonPesanStyle ${prefs?.getOutletCart() == null}")
            binding.buttonPesan.apply {
                isEnabled = false
                isClickable = false
//                backgroundTintList = resources.getColorStateList(R.color.dark_gray)
//                setTextColor(R.color.disabledColor)
            }
        } else {
            if (cartLayananList.size > 0) {
                binding.buttonPesan.apply {
                    isEnabled = true
                    isClickable = true
                    binding.buttonPesan.setOnClickListener {
                        viewModel.addTransakis(
                            prefs?.getOutletCart()!!,
                            cartLayananList,
                            grandTotal,
                            promoChecked?.id ?: null,
                            null
                        )
                    }
                }
            }
        }
    }

    fun outletBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingOutletBottomSheet = OutletBotomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingOutletBottomSheet.root)
        outletSheetBehavior = (dialog as BottomSheetDialog).behavior
        outletSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        outletSheetBehavior.isHideable = true
        bindingOutletBottomSheet.rvOutlet.layoutManager = LinearLayoutManager(dialog.context)
        bindingOutletBottomSheet.rvOutlet.adapter = outletAdapter
        bindingOutletBottomSheet.inputSearchOutlet.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.getOutlet(s.toString())
            }
        })
        return dialog
    }

    fun promoBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingPromoBottomSheet = PromoBotomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingPromoBottomSheet.root)
        promoSheetBehavior = (dialog as BottomSheetDialog).behavior
        promoSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        promoSheetBehavior.isHideable = true
        bindingPromoBottomSheet.rvPromo.layoutManager = LinearLayoutManager(dialog.context)
        bindingPromoBottomSheet.rvPromo.adapter = promoAdapter
        bindingPromoBottomSheet.inputSearchPromo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.getPromo(s.toString())
            }
        })
        return dialog
    }

    fun cekPromo(promo: Promo?, cart: Cart): Int {
        val layananPromo: ArrayList<String> =
            promo?.detailPromo?.map { it.layanan.id }?.toCollection(ArrayList()) ?: ArrayList()
        return if (layananPromo.contains(cart.layanan.id)) {
            if (promo != null) {
                if (promo.jenisPromo == "quantity" && cart.satuan >= promo.syaratMinimal!! || promo.jenisPromo == "harga" && totalHarga >= promo.syaratMinimal!!) {
                    if (promo.jenisDiskon == "nominal")
                        promo.jumlahDiskon!!
                    else
                        (cart.totalHarga * promo.jumlahDiskon!! / 100)
                } else if (promo.jenisPromo == "tanpa") {
                    if (promo.jenisDiskon === "nominal")
                        promo.jumlahDiskon!!
                    else
                        (cart.totalHarga * promo.jumlahDiskon!! / 100)
                } else
                    0
            } else
                0
        } else
            0
    }

    fun bayarBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingBayarBottomSheetBinding = BayarBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingBayarBottomSheetBinding.root)
        bindingBayarBottomSheetBinding.textGrandTotal.text = "Rp. $grandTotal"
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        imgCaptureExecutor = Executors.newSingleThreadExecutor()
        kameraInput()
        bayarSheetBehavior = (dialog as BottomSheetDialog).behavior
        bayarSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bayarSheetBehavior.isHideable = true
        bindingBayarBottomSheetBinding.buttonTake.setOnClickListener {
            if (bindingBayarBottomSheetBinding.buttonTake.text == "GANTI GAMBAR") {
                bindingBayarBottomSheetBinding.apply {
                    previewCamera.visibility = View.VISIBLE
                    imageBuktiPembayaran.visibility = View.INVISIBLE
                    buttonBayar.visibility = View.GONE
                    buttonTake.setText("AMBIL GAMBAR")
                }
            } else {
                takePhoto()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    animateFlash()
                }
            }

        }
        bindingBayarBottomSheetBinding.buttonBayar.setOnClickListener {
            image = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date()) + ".jpg"
            viewModel.bayarTransaksi(transaksiId, image, imageFile)
            Toast.makeText(this, "Sukses Pembayaran Laundry", Toast.LENGTH_SHORT)
            dialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { finish() }
        }
        bindingBayarBottomSheetBinding.buttonCancel.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { finish() }
        }
        return dialog
    }

    fun kameraInput() =
        runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA) {
            bindingBayarBottomSheetBinding.previewCamera.setOnClickListener {
                cameraSelector = if (cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA) {
                    CameraSelector.DEFAULT_FRONT_CAMERA
                } else {
                    CameraSelector.DEFAULT_BACK_CAMERA
                }
                startCamera()
            }

            startCamera()
        }

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraSelector: CameraSelector
    private var imageCapture: ImageCapture? = null
    private lateinit var imgCaptureExecutor: ExecutorService

    private fun startCamera() {
        bindingBayarBottomSheetBinding.apply {
            previewCamera.visibility = View.VISIBLE
            buttonTake.visibility = View.VISIBLE
        }
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(bindingBayarBottomSheetBinding.previewCamera.createSurfaceProvider())
        }
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            imageCapture = ImageCapture.Builder().build()

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            } catch (e: Exception) {
            }
        }, ContextCompat.getMainExecutor(this))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun animateFlash() {
        binding.root.postDelayed({
            binding.root.foreground = ColorDrawable(Color.WHITE)
            binding.root.postDelayed({
                binding.root.foreground = null
            }, 50)
        }, 100)
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        val name = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            .format(System.currentTimeMillis())
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(ContentValues.TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    fileUri = output.savedUri
                    Picasso.get().load(fileUri).into(bindingBayarBottomSheetBinding.imageBuktiPembayaran)
                    bindingBayarBottomSheetBinding.apply {
                        previewCamera.visibility = View.INVISIBLE
                        imageBuktiPembayaran.visibility = View.VISIBLE
                        buttonBayar.visibility = View.VISIBLE
                        buttonTake.setText("GANTI GAMBAR")
                    }
                    imageFile = encodeImageToString(fileUri)
                }
            }
        )
    }

    fun encodeImageToString(uri: Uri): String {
        val imageStream: InputStream? = contentResolver.openInputStream(uri)
        val selectedImage = BitmapFactory.decodeStream(imageStream)
        val outputStream = ByteArrayOutputStream()
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

}
