package com.example.detergentapp.ui.main.order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.detergentapp.App
import com.example.detergentapp.data.retrofit.ApiConfig
import com.example.detergentapp.data.retrofit.service.TransaksiService
import com.example.detergentapp.entity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class OrderViewModel : ViewModel() {
    private val orderList = MutableLiveData<List<Transaksi>>()
    val getOrderList: LiveData<List<Transaksi>?>
        get() = orderList
    private val ulasan = MutableLiveData<Ulasan>()
    val getUlasan: LiveData<Ulasan?>
        get() = ulasan
    private val isBayar = MutableLiveData<Boolean>()
    val getBayar: LiveData<Boolean>
        get() = isBayar
    private val isStatus = MutableLiveData<Boolean>()
    val getStatus: LiveData<Boolean>
        get() = isStatus

    fun orderList(idPelanggan: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.getTransaksiList(idPelanggan)
        call.enqueue(object : Callback<ResponseData<List<Transaksi>>?> {
            override fun onResponse(
                call: Call<ResponseData<List<Transaksi>>?>,
                response: Response<ResponseData<List<Transaksi>>?>
            ) {
                Timber.d("onResponse: ${response.body()?.data?.get(9)?.detailTracking}")
                if (response.isSuccessful) {
                    orderList.value =
                        response.body()?.data?.sortedByDescending { it.updateddAt }
                            ?.sortedWith(compareBy({ it.updateddAt }, { it.statusProses != "cancel" }))
                } else {
                    orderList.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<List<Transaksi>>?>, t: Throwable) {
                Timber.e(t.message)
                orderList.value = null
            }

        })
    }

    fun addUlasan(rate: Ulasan) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.postTambahUlasan(
            rate
        )
        call.enqueue(object : Callback<ResponseData<Ulasan>?> {
            override fun onResponse(
                call: Call<ResponseData<Ulasan>?>,
                response: Response<ResponseData<Ulasan>?>
            ) {
                Timber.d("response : $response")
                if (response.isSuccessful) {
                    ulasan.value = response.body()?.data
                } else {
                    ulasan.value = null
                }
            }

            override fun onFailure(call: Call<ResponseData<Ulasan>?>, t: Throwable) {
                ulasan.value = null
                Timber.e("error : $t  $call")
            }
        })
    }

    fun bayarTransaksi(id: String, foto: String, file: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.postBayarTransaksi(
            id,
            foto,
            file
        )
        call.enqueue(object : Callback<ResponseData<Transaksi>?> {
            override fun onResponse(
                call: Call<ResponseData<Transaksi>?>,
                response: Response<ResponseData<Transaksi>?>
            ) {
                if (response.isSuccessful) {
                    if (response.body()?.status == true) {
                        Timber.d("response : $response")
                        isBayar.value == true
                    } else
                        isBayar.value == false
                } else {
                    isBayar.value = false
                }
            }

            override fun onFailure(call: Call<ResponseData<Transaksi>?>, t: Throwable) {
                isBayar.value = false
                Timber.e("error : $t  $call")
            }

        })
    }

    fun statusTransaksi(id: String, status: String) {
        val retroInstance = ApiConfig.getRetroInstance().create(TransaksiService::class.java)
        val call = retroInstance.postStatusTransaksi(
            id,
            status
        )
        call.enqueue(object : Callback<ResponseData<Transaksi>?> {
            override fun onResponse(
                call: Call<ResponseData<Transaksi>?>,
                response: Response<ResponseData<Transaksi>?>
            ) {
                if (response.isSuccessful) {
                    if (response.body()?.status == true) {
                        Timber.d("response : $response")
                        isStatus.value == true
                    } else
                        isStatus.value == false
                } else {
                    isStatus.value = false
                }
            }

            override fun onFailure(call: Call<ResponseData<Transaksi>?>, t: Throwable) {
                isStatus.value = false
                Timber.e("error : $t  $call")
            }

        })
    }
}
