package com.example.detergentapp.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.detergentapp.App
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.App.Companion.tokenFCM
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityLoginBinding
import com.example.detergentapp.databinding.LoginBottomSheetDialogBinding
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.ui.main.MainActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import timber.log.Timber

class LoginActivity : AppCompatActivity() {
    private var emailValid = false
    private var passwordValid = false
    lateinit var binding: ActivityLoginBinding
    lateinit var viewModel: AuthViewModel
    private lateinit var bindingBottomSheet: LoginBottomSheetDialogBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        observer()
        binding.buttonLogin.setOnClickListener {
            loginBottomSheet().show()
        }
        binding.buttonRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        authCheck()
    }

    private fun authCheck() {
        if (prefs?.id != null) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { finish() }
        }
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        viewModel.getPelanggan.observe(this, Observer<Pelanggan?> {
            Timber.d("message : $it")
            if (it == null) {
                Toast.makeText(this, "Gagal Login", Toast.LENGTH_LONG).show()
            } else {
                prefs?.saveUser(it)
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent).also { finish() }
            }
        })
    }

    private fun initView() {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun loginBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingBottomSheet = LoginBottomSheetDialogBinding.inflate(layoutInflater, binding.root, false)
        validateButton()
        dialog.setContentView(bindingBottomSheet.root)
        dialog.setCancelable(true)
        bindingBottomSheet.editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateEmail()
            }
        })
        bindingBottomSheet.editPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validatePassword()
            }
        })
        bindingBottomSheet.buttonLogin.setOnClickListener {
            viewModel.login(
                bindingBottomSheet.editEmail.text.toString(),
                bindingBottomSheet.editPassword.text.toString()
            )
        }
        return dialog
    }

    fun validateEmail() {
        // jika password tidak valid tampilkan peringatan
        val input = bindingBottomSheet.editEmail.text.toString()
        if (!Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
            emailValid = false
            showEmailExistAlert(true)
        } else {
            emailValid = true
            showEmailExistAlert(false)
        }
        validateButton()
    }

    private fun validatePassword() {
        // jika password < 6 karakter tampilkan peringatan
        val input = bindingBottomSheet.editPassword.text.toString()
        if (input.length < 6) {
            passwordValid = false
            showPasswordMinimalAlert(true)
        } else {
            passwordValid = true
            showPasswordMinimalAlert(false)
        }
        validateButton()
    }

    private fun validateButton() {
        // jika semua field sudah terisi, enable button submit
        if (emailValid && passwordValid) {
            bindingBottomSheet.buttonLogin.isEnabled = true
            bindingBottomSheet.buttonLogin.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
        } else {
            bindingBottomSheet.buttonLogin.isEnabled = false
            bindingBottomSheet.buttonLogin.setBackgroundColor(ContextCompat.getColor(this, android.R.color.darker_gray))
        }
    }

    private fun showEmailExistAlert(isNotValid: Boolean) {
        bindingBottomSheet.editEmail.error = if (isNotValid) "Email anda tidak sesuai" else null
    }

    private fun showPasswordMinimalAlert(isNotValid: Boolean) {
        bindingBottomSheet.editPassword.error = if (isNotValid) "Password anda tidak sesuai" else null
    }
}
