package com.example.detergentapp.ui.main.home

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.databinding.ItemOrderHomeBinding
import com.example.detergentapp.entity.Transaksi
import timber.log.Timber


class OrderListAdapter(
    private val context: Context,
    var orderItemList: List<Transaksi>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<OrderListAdapter.OrderItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OrderItemViewHolder {
        val binding: ItemOrderHomeBinding =
            ItemOrderHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OrderItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int) {
        holder.bind(orderItemList[position], holder.adapterPosition, listener as OnItemClickListener)
    }

    override fun getItemCount(): Int {
        return orderItemList.size
    }


    class OrderItemViewHolder(var binding: ItemOrderHomeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(order: Transaksi, position: Int, listener: OnItemClickListener) {
            binding.textOutletOrderList.text = order.outlet?.nama
            binding.textDateOrderList.text = order.createdAt
            binding.textPriceOrderList.text = "Rp. ${order.grandTotal}"
            binding.textStatusOrderList.text = order.statusProses
            binding.textStatusOrderList.setTextColor(statusColor(order.statusProses))
            val drawables = binding.textStatusOrderList.compoundDrawables
            drawables.forEach {
                if (it != null) {
                    val wrappedDrawable = DrawableCompat.wrap(it)
                    DrawableCompat.setTint(wrappedDrawable, statusColor(order.statusProses))
                }
            }
            binding.root.setOnClickListener { listener.onItemClick(position) }
            binding.buttonTrackOrderList.setOnClickListener { listener.onTrackOrder(position) }
        }

        private fun statusColor(status: String?): Int {
            return when (status) {
                "pesan" -> Color.parseColor("#FF7000")
                "konfirmasi" -> Color.parseColor("#FFBF00")
                "penjemputan" -> Color.parseColor("#0F78CB")
                "antrian" -> Color.parseColor("#2146C7")
                "cuci" -> Color.parseColor("#D5CEA3")
                "setrika" -> Color.parseColor("#3C2A21")
                "ambil" -> Color.parseColor("#D09CFA")
                "antar" -> Color.parseColor("#A555EC")
                "selesai" -> Color.parseColor("#54B435")
                "cancel" -> Color.parseColor("#FF6464")
                else -> Color.parseColor("#222222")
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
        fun onTrackOrder(position: Int)
    }
}
