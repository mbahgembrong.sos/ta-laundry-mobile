package com.example.detergentapp.ui.auth

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.detergentapp.App
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityRegisterBinding
import com.example.detergentapp.databinding.CameraBottomSheetBinding
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.entity.Promo
import com.example.detergentapp.ui.main.MainActivity
import com.example.detergentapp.ui.main.home.LayananPromoAdapter
import com.example.detergentapp.utils.MediaHelper
import com.example.detergentapp.utils.PhotoHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.common.util.concurrent.ListenableFuture
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import mumayank.com.airlocationlibrary.AirLocation
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class RegisterActivity : AppCompatActivity() {
    lateinit var binding: ActivityRegisterBinding
    lateinit var viewModel: AuthViewModel
    var jenisKelamin = ""
    lateinit var mediaHelper: MediaHelper
    var selectMedia = true
    lateinit var image: String
    lateinit var imageFile: String
    var fileUri = Uri.parse("")
    var lat: Double = 0.0;
    var lng: Double = 0.0;
    var airLoc: AirLocation? = null
    lateinit var bindingCameraBottomSheetBinding: CameraBottomSheetBinding
    lateinit var cameraSheetBehavior: BottomSheetBehavior<*>

    init {
        mediaHelper = MediaHelper(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        observer()
        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        binding.buttonRegister.setOnClickListener {
            image = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date()) + ".jpg"
            viewModel.register(
                Pelanggan(
                    UUID.randomUUID().toString(),
                    binding.editNama.text.toString(),
                    binding.editEmail.text.toString(),
                    jenisKelamin,
                    image,
                    binding.editTelp.text.toString(),
                    binding.editAlamat.text.toString(),
                    binding.editPassword.text.toString(),
                    lng,
                    lat,
                    App.tokenFCM,
                    imageFile,
                )
            )
        }
    }

    fun getLocation() = runWithPermissions(android.Manifest.permission.ACCESS_FINE_LOCATION) {
        airLoc = AirLocation(this, true, true,
            object : AirLocation.Callbacks {
                override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                    Toast.makeText(
                        applicationContext, "Gagal mendapatkan posisi saat ini",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onSuccess(location: Location) {
                    lat = location.latitude
                    lng = location.longitude
                    Timber.e("lat: $lat, lng: $lng")
                    val geocoder = Geocoder(applicationContext, Locale.getDefault())
                    var addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                    Timber.d("alamat: ${addresses?.get(0)?.getAddressLine(0)}")
                    binding.editAlamat.setText(addresses?.get(0)?.getAddressLine(0))
                }
            })
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        viewModel.getPelanggan.observe(this, Observer<Pelanggan?> {
            if (it == null) {
                Toast.makeText(this, "Gagal Register", Toast.LENGTH_LONG).show()
            } else {
                App.prefs?.saveUser(it)
                onBackPressed()
            }
        })
    }

    private fun initView() {
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.radioJenisKelamin.setOnCheckedChangeListener { radioGroup, i ->
            val radioGroup = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            jenisKelamin = if (radioGroup.text == "Perempuan") "P" else "L"
        }
        binding.radioImage.setOnCheckedChangeListener { radioGroup, i ->
            val radioGroup = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            if (radioGroup.text == "Storage") {
                selectMedia = true
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(intent, mediaHelper.getRcGallery())
            } else {
                cameraBottomSheet().show()

            }
        }
        getLocation()
    }

    fun cameraBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingCameraBottomSheetBinding = CameraBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingCameraBottomSheetBinding.root)
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        imgCaptureExecutor = Executors.newSingleThreadExecutor()
        kameraInput()
        cameraSheetBehavior = (dialog as BottomSheetDialog).behavior
        cameraSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        cameraSheetBehavior.isHideable = true
        bindingCameraBottomSheetBinding.buttonTakeCamera.setOnClickListener {
            takePhoto()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                animateFlash()
                dialog.dismiss()
            }
        }
        return dialog
    }

    fun kameraInput() =
        runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA) {
            bindingCameraBottomSheetBinding.previewCamera.setOnClickListener {
                cameraSelector = if (cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA) {
                    CameraSelector.DEFAULT_FRONT_CAMERA
                } else {
                    CameraSelector.DEFAULT_BACK_CAMERA
                }
                startCamera()
            }

            startCamera()
        }

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraSelector: CameraSelector
    private var imageCapture: ImageCapture? = null
    private lateinit var imgCaptureExecutor: ExecutorService

    private fun startCamera() {
        bindingCameraBottomSheetBinding.apply {
            previewCamera.visibility = View.VISIBLE
            buttonTakeCamera.visibility = View.VISIBLE
        }
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(bindingCameraBottomSheetBinding.previewCamera.createSurfaceProvider())
        }
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            imageCapture = ImageCapture.Builder().build()

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            } catch (e: Exception) {
            }
        }, ContextCompat.getMainExecutor(this))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun animateFlash() {
        binding.root.postDelayed({
            binding.root.foreground = ColorDrawable(Color.WHITE)
            binding.root.postDelayed({
                binding.root.foreground = null
            }, 50)
        }, 100)
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        val name = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            .format(System.currentTimeMillis())
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(ContentValues.TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    fileUri = output.savedUri
                    Picasso.get().load(fileUri).into(binding.imageRegister)
                    binding.imageRegister.visibility = View.VISIBLE
                    imageFile = encodeImageToString(fileUri)
                }
            }
        )
    }

    fun encodeImageToString(uri: Uri): String {
        val imageStream: InputStream? = contentResolver.openInputStream(uri)
        val selectedImage = BitmapFactory.decodeStream(imageStream)
        val outputStream = ByteArrayOutputStream()
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == mediaHelper.getRcGallery() && selectMedia) {
                imageFile = mediaHelper.getBitmapToString(data?.data, binding.imageRegister)
                binding.imageRegister.visibility = View.VISIBLE
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
