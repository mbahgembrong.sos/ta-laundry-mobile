package com.example.detergentapp.ui.main.profile

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.detergentapp.App
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.R
import com.example.detergentapp.databinding.CameraBottomSheetBinding
import com.example.detergentapp.databinding.FragmentProfileBinding
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.ui.auth.LoginActivity
import com.example.detergentapp.utils.AppConstants.URL_IMAGE_PELANGGAN
import com.example.detergentapp.utils.MediaHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.common.util.concurrent.ListenableFuture
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import mumayank.com.airlocationlibrary.AirLocation
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding
    lateinit var viewModel: ProfileViewModel
    lateinit var user: Pelanggan
    var jenisKelamin = ""

    lateinit var mediaHelper: MediaHelper
    var selectMedia = true
    var image: String? = null
    var imageFile: String? = null
    var fileUri = Uri.parse("")
    var lat: Double = 0.0;
    var lng: Double = 0.0;
    var airLoc: AirLocation? = null
    lateinit var bindingCameraBottomSheetBinding: CameraBottomSheetBinding
    lateinit var cameraSheetBehavior: BottomSheetBehavior<*>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mediaHelper = MediaHelper(requireContext())
        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        image = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date()) + ".jpg"
        binding.buttonUpdate.setOnClickListener {
            viewModel.update(
                Pelanggan(
                    prefs?.id,
                    binding.editName.text.toString(),
                    binding.editEmail.text.toString(),
                    jenisKelamin,
                    image,
                    prefs?.telp,
                    binding.editAlamatProfile.text.toString(),
                    if (binding.editPassword.text.toString()
                            .isNotEmpty()
                    ) binding.editPassword.text.toString() else null,
                    if (lng == 0.0 && prefs?.longtitude != "null") prefs?.longtitude?.toDouble() else lng,
                    if (lat == 0.0 && prefs?.latitude != "null") prefs?.latitude?.toDouble() else lat,
                    App.tokenFCM,
                    imageFile
                )

            )
        }
        binding.buttonLogout.setOnClickListener {
            prefs?.clearPreferences()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { activity?.finish() }
        }


//        getLocation()
    }

    fun getLocation() {
        airLoc = AirLocation(requireActivity(), true, true,
            object : AirLocation.Callbacks {
                override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                    Toast.makeText(
                        requireContext(), "Gagal mendapatkan posisi saat ini",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onSuccess(location: Location) {
                    lat = location.latitude
                    lng = location.longitude
                    Timber.e("lat: $lat, lng: $lng")
                    val geocoder = Geocoder(requireContext(), Locale.getDefault())
                    var addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                    binding.editAlamatProfile.setText(addresses?.get(0)?.getAddressLine(0))
                }
            })
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        viewModel.getPelanggan.observe(this) {
            if (it == null) {
                Toast.makeText(requireContext(), "Error Update Profile", Toast.LENGTH_LONG).show()
            } else {
                prefs?.saveUser(it)
                Toast.makeText(requireContext(), "Sukses Update Profile", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun initView() {
        user = prefs?.getUser()!!
        Picasso.get().load(URL_IMAGE_PELANGGAN + user.foto).placeholder(R.drawable.ic_signup).into(binding.imageProfile)
        binding.editName.setText(user.nama)
        binding.editEmail.setText(user.email)
        jenisKelamin = user.jenisKelamin.toString()
        if (user.jenisKelamin == "P") binding.radioPerempuan.isChecked = true
        else binding.radioPria.isChecked = true
        binding.editAlamatProfile.setText(user.alamat)
        binding.radioJenisKelamin.setOnCheckedChangeListener { radioGroup, i ->
            val radioGroup = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            jenisKelamin = if (radioGroup.text == "Perempuan") "P" else "L"
        }
        binding.radioImage.setOnCheckedChangeListener { radioGroup, i ->
            val radioGroup = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            if (radioGroup.text == "Storage") {
                selectMedia = true
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(intent, mediaHelper.getRcGallery())
            } else {
                cameraBottomSheet().show()
            }
        }
        binding.radioAlamatTetap.isChecked = true
        binding.radioGroupAlamat.setOnCheckedChangeListener { radioGroup, i ->
            val radioGroup = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            if (radioGroup.text == "Alamat Lama") {
                lng = user.longtitude?.toDouble()!!
                lat = user.latitude?.toDouble()!!
                binding.editAlamatProfile.setText(user.alamat)
            } else {
                getLocation()
            }
        }
        if (prefs?.longtitude == "null" || user.longtitude == null) {
            binding.radioGaniALamat.isChecked = true
            getLocation()
        }
    }

    fun cameraBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(requireContext())
        bindingCameraBottomSheetBinding = CameraBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingCameraBottomSheetBinding.root)
        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        imgCaptureExecutor = Executors.newSingleThreadExecutor()
        kameraInput()
        cameraSheetBehavior = (dialog as BottomSheetDialog).behavior
        cameraSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        cameraSheetBehavior.isHideable = true
        bindingCameraBottomSheetBinding.buttonTakeCamera.setOnClickListener {
            takePhoto()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                animateFlash()
                dialog.dismiss()
            }
        }
        return dialog
    }

    fun kameraInput() =
        runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA) {
            bindingCameraBottomSheetBinding.previewCamera.setOnClickListener {
                cameraSelector = if (cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA) {
                    CameraSelector.DEFAULT_FRONT_CAMERA
                } else {
                    CameraSelector.DEFAULT_BACK_CAMERA
                }
                startCamera()
            }

            startCamera()
        }

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraSelector: CameraSelector
    private var imageCapture: ImageCapture? = null
    private lateinit var imgCaptureExecutor: ExecutorService

    private fun startCamera() {
        bindingCameraBottomSheetBinding.apply {
            previewCamera.visibility = View.VISIBLE
            buttonTakeCamera.visibility = View.VISIBLE
        }
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(bindingCameraBottomSheetBinding.previewCamera.createSurfaceProvider())
        }
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            imageCapture = ImageCapture.Builder().build()

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            } catch (e: Exception) {
            }
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun animateFlash() {
        binding.root.postDelayed({
            binding.root.foreground = ColorDrawable(Color.WHITE)
            binding.root.postDelayed({
                binding.root.foreground = null
            }, 50)
        }, 100)
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        val name = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            .format(System.currentTimeMillis())
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                requireContext().contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(ContentValues.TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    fileUri = output.savedUri
                    Picasso.get().load(fileUri).into(binding.imageProfile)
                    binding.imageProfile.visibility = View.VISIBLE
                    imageFile = encodeImageToString(fileUri)
                }
            }
        )
    }

    fun encodeImageToString(uri: Uri): String {
        val imageStream: InputStream? = requireContext().contentResolver.openInputStream(uri)
        val selectedImage = BitmapFactory.decodeStream(imageStream)
        val outputStream = ByteArrayOutputStream()
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLoc?.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == mediaHelper.getRcGallery() && selectMedia) {
                imageFile = mediaHelper.getBitmapToString(data?.data, binding.imageProfile)
                binding.imageProfile.visibility = View.VISIBLE

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}
