package com.example.detergentapp.ui.main.order

import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.detergentapp.App
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ActivityOrderDetailBinding
import com.example.detergentapp.databinding.BayarBottomSheetBinding
import com.example.detergentapp.databinding.RateBottomSheetBinding
import com.example.detergentapp.entity.DetailTracking
import com.example.detergentapp.entity.DetailTransaksiLayanan
import com.example.detergentapp.entity.Transaksi
import com.example.detergentapp.entity.Ulasan
import com.example.detergentapp.ui.main.MainActivity
import com.example.detergentapp.ui.transaction.CartLayananAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.common.util.concurrent.ListenableFuture
import com.google.zxing.BarcodeFormat
import com.hsalf.smileyrating.SmileyRating
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class OrderDetailActivity : AppCompatActivity() {
    lateinit var binding: ActivityOrderDetailBinding
    lateinit var statusAdapter: StatusTrackingAdapter
    lateinit var layananAdapter: LayananTrackingAdapter
    lateinit var transaksi: Transaksi
    lateinit var viewModel: OrderViewModel
    lateinit var ulasan: Ulasan
    lateinit var bindingBayarBottomSheetBinding: BayarBottomSheetBinding
    lateinit var bayarSheetBehavior: BottomSheetBehavior<*>
    lateinit var image: String
    lateinit var imageFile: String
    var fileUri = Uri.parse("")
    lateinit var transaksiId: String

    companion object {
        const val DETAIL_TRACKING = "detail_tracking"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        transaksi = intent.getParcelableExtra<Transaksi>(DETAIL_TRACKING)!!
        Timber.d("transaksi: ${transaksi.detailTransaksiLayanan}")
        transaksiId = transaksi.id!!
        initView()
        observer()
        binding.buttonUlasan.setOnClickListener {
            showRatingDialog()
        }
        binding.buttonCancel.setOnClickListener {
            AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle("Apakah kamu yakin?")
                .setPositiveButton("Ya") { dialog, which ->
                    viewModel.statusTransaksi(transaksiId, "cancel")
                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent).also { finish() }
                }
                .setNegativeButton("Tidak") { dialog, which ->
                    dialog.dismiss()
                }.show()
        }
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(OrderViewModel::class.java)
        viewModel.getUlasan.observe(this) {
            if (it != null)
                ulasanView(it)
        }
        viewModel.getBayar.observe(this) {
            Timber.d("bayar")
            if (it == true) {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent).also { finish() }
            }
        }
        viewModel.getStatus.observe(this) {
            Timber.d("camcel")
            if (it == true) {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent).also { finish() }
            }
        }
    }

    private fun initView() {
        var diskon = 0
        binding.textNamaOutletTransaksi.text = transaksi.outlet?.nama ?: ""
        binding.textOrderTime.text = transaksi.createdAt
        binding.textItemTotalPrice.text = "Rp. ${transaksi.grandTotal}"
        binding.textKeterangan.text = transaksi.keterangan ?: "Detergent Laundry"
        if (transaksi.promo != null) {
            if (transaksi.promo!!.jenisDiskon == "nominal") diskon = transaksi.promo!!.jumlahDiskon else diskon =
                (transaksi.promo!!.jumlahDiskon / 100) * transaksi.grandTotal!!.toInt()
            binding.textDiskon.text = "Rp. $diskon"
            binding.layoutDiskonDetail.visibility = View.VISIBLE
        }
        binding.textGrandTotalPrice.text = "Rp. ${transaksi.grandTotal!!.toInt() - diskon}"
        val barcodeEncoder = BarcodeEncoder()
        val bitmap = barcodeEncoder.encodeBitmap(transaksi.id, BarcodeFormat.QR_CODE, 400, 400)
        binding.imageQrCode.setImageBitmap(bitmap)
        if (transaksi.statusProses == "selesai")
            binding.buttonUlasan.visibility = View.VISIBLE
        if (transaksi.ulasan != null)
            ulasanView(transaksi.ulasan!!)
        if (transaksi.statusPembayaran == "belum lunas" && transaksi.statusProses != "cancel") {
            bayarBottomSheet().show()
        }
//        'pesan', 'konfirmasi', 'penjemputan', 'antrian'
        val statusCancel = arrayListOf<String>("pesan", "konfirmasi", "penjemputan", "antrian")
        if (transaksi.statusProses in statusCancel)
            binding.buttonCancel.visibility = View.VISIBLE
        setupRecyclerView()
    }

    fun ulasanView(ulasan: Ulasan) {
        this.ulasan = ulasan
        binding.layoutUlasan.visibility = View.VISIBLE
        binding.textStarULasan.text = "${ulasan.star}"
        binding.textUlasan.text = ulasan.ulasan
        binding.buttonUlasan.visibility = View.GONE
    }

    private fun setupRecyclerView() {
        statusAdapter =
            StatusTrackingAdapter(applicationContext,
                transaksi.detailTracking!!.sortedByDescending { it?.createdAt } as List<DetailTracking>)
        binding.recyclerStatus.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerStatus.adapter = statusAdapter
        layananAdapter = LayananTrackingAdapter(applicationContext, transaksi.detailTransaksiLayanan)
        binding.recyclerOrderItems.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerOrderItems.adapter = layananAdapter
    }

    var rating = 0.0
    private fun showRatingDialog() {
        val dialogBinding: RateBottomSheetBinding =
            RateBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(dialogBinding.root)
        dialog.show()
        Handler().postDelayed({
            dialogBinding.smileyRating.setRating(SmileyRating.Type.GOOD, true)
        }, 200)
        dialogBinding.buttonRate.setOnClickListener {
            var smiley = dialogBinding.smileyRating.selectedSmiley
            rating = 0.0
            when (smiley) {
                SmileyRating.Type.TERRIBLE -> {
                    rating = 1.0
                }

                SmileyRating.Type.BAD -> {
                    rating = 2.0
                }

                SmileyRating.Type.OKAY -> {
                    rating = 3.0
                }

                SmileyRating.Type.GOOD -> {
                    rating = 4.0
                }

                SmileyRating.Type.GREAT -> {
                    rating = 5.0
                }
            }
            val transaksiId = transaksi?.id
            val outletId = transaksi.outlet?.id
            val feedback = dialogBinding.editFeedback.text.toString()
            viewModel.addUlasan(
                Ulasan(transaksiId, feedback, rating, outletId)
            )
            dialog.dismiss()
        }
    }

    fun bayarBottomSheet(): BottomSheetDialog {
        val dialog = BottomSheetDialog(this)
        bindingBayarBottomSheetBinding = BayarBottomSheetBinding.inflate(layoutInflater, binding.root, false)
        dialog.setContentView(bindingBayarBottomSheetBinding.root)
        bindingBayarBottomSheetBinding.textGrandTotal.text = "Rp. ${transaksi.grandTotal}"
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        imgCaptureExecutor = Executors.newSingleThreadExecutor()
        kameraInput()
        bayarSheetBehavior = (dialog as BottomSheetDialog).behavior
        bayarSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bayarSheetBehavior.isHideable = true
        bindingBayarBottomSheetBinding.buttonTake.setOnClickListener {
            if (bindingBayarBottomSheetBinding.buttonTake.text == "GANTI GAMBAR") {
                bindingBayarBottomSheetBinding.apply {
                    previewCamera.visibility = View.VISIBLE
                    imageBuktiPembayaran.visibility = View.INVISIBLE
                    buttonBayar.visibility = View.GONE
                    buttonTake.setText("AMBIL GAMBAR")
                }
            } else {
                takePhoto()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    animateFlash()
                }
            }

        }
        bindingBayarBottomSheetBinding.buttonBayar.setOnClickListener {
            image = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date()) + ".jpg"
            viewModel.bayarTransaksi(transaksiId, image, imageFile)
            Toast.makeText(this, "Sukses Pembayaran Laundry", Toast.LENGTH_SHORT)
            dialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent).also { finish() }
        }
        bindingBayarBottomSheetBinding.buttonCancel.setOnClickListener {
            dialog.dismiss()

        }
        return dialog
    }

    fun kameraInput() =
        runWithPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA) {
            bindingBayarBottomSheetBinding.previewCamera.setOnClickListener {
                cameraSelector = if (cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA) {
                    CameraSelector.DEFAULT_FRONT_CAMERA
                } else {
                    CameraSelector.DEFAULT_BACK_CAMERA
                }
                startCamera()
            }

            startCamera()
        }

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var cameraSelector: CameraSelector
    private var imageCapture: ImageCapture? = null
    private lateinit var imgCaptureExecutor: ExecutorService

    private fun startCamera() {
        bindingBayarBottomSheetBinding.apply {
            previewCamera.visibility = View.VISIBLE
            buttonTake.visibility = View.VISIBLE
        }
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(bindingBayarBottomSheetBinding.previewCamera.createSurfaceProvider())
        }
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            imageCapture = ImageCapture.Builder().build()

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            } catch (e: Exception) {
            }
        }, ContextCompat.getMainExecutor(this))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun animateFlash() {
        binding.root.postDelayed({
            binding.root.foreground = ColorDrawable(Color.WHITE)
            binding.root.postDelayed({
                binding.root.foreground = null
            }, 50)
        }, 100)
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        val name = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            .format(System.currentTimeMillis())
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/CameraX-Image")
            }
        }
        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()
        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(ContentValues.TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    fileUri = output.savedUri
                    Picasso.get().load(fileUri).into(bindingBayarBottomSheetBinding.imageBuktiPembayaran)
                    bindingBayarBottomSheetBinding.apply {
                        previewCamera.visibility = View.INVISIBLE
                        imageBuktiPembayaran.visibility = View.VISIBLE
                        buttonBayar.visibility = View.VISIBLE
                        buttonTake.setText("GANTI GAMBAR")
                    }
                    imageFile = encodeImageToString(fileUri)
                }
            }
        )
    }

    fun encodeImageToString(uri: Uri): String {
        val imageStream: InputStream? = contentResolver.openInputStream(uri)
        val selectedImage = BitmapFactory.decodeStream(imageStream)
        val outputStream = ByteArrayOutputStream()
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, outputStream)
        val byteArray = outputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }
}
