package com.example.detergentapp.ui.main.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ItemLayananHomeBinding
import com.example.detergentapp.entity.DetailPromo
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.utils.AppConstants
import com.squareup.picasso.Picasso

class LayananPromoAdapter(
    private val context: Context,
    private val layananItemList: List<DetailPromo>
) : RecyclerView.Adapter<LayananPromoAdapter.LayananItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LayananItemViewHolder {
        val binding: ItemLayananHomeBinding =
            ItemLayananHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LayananItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LayananItemViewHolder, position: Int) {
        holder.bind(layananItemList[position], holder.adapterPosition)
    }

    override fun getItemCount(): Int {
        return layananItemList.size
    }

    class LayananItemViewHolder(var binding: ItemLayananHomeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(layanan: DetailPromo, position: Int) {
            if (!layanan.layanan.foto.isNullOrEmpty())
                Picasso.get().load("${AppConstants.URL_IMAGE_LAYANAN}${layanan.layanan.foto}")
                    .placeholder(R.drawable.ic_orders).error(R.drawable.ic_orders)
                    .into(binding.imageItemLayanan)
            binding.namaItemLayanan.text = layanan.layanan.nama
            binding.buttonTambah.visibility = View.GONE
        }
    }

}
