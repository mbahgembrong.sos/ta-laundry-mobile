package com.example.detergentapp.ui.transaction

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ItemCartLayananBinding
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.utils.CartHelper
import com.squareup.picasso.Picasso
import timber.log.Timber

class CartLayananAdapter(
    private val context: Context,
    private val cartItemList: List<Cart>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<CartLayananAdapter.CartItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CartItemViewHolder {
        val binding: ItemCartLayananBinding =
            ItemCartLayananBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartItemViewHolder, position: Int) {
        holder.bind(cartItemList[position], holder.adapterPosition, listener as OnItemClickListener)
    }

    override fun getItemCount(): Int {
        return cartItemList.size
    }

    class CartItemViewHolder(var binding: ItemCartLayananBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cart: Cart, position: Int, listener: OnItemClickListener) {
            binding.textLayananName.text = cart.layanan.nama
            binding.textHargaLayanan.text = "Rp. ${cart.layanan.harga}"
            binding.textJenisSatuan.text = cart.layanan.satuanHitung
            binding.layoutQuantityControl.textQuantity.text = cart.satuan.toString()
            binding.layoutQuantityControl.imageAdd.setOnClickListener {
                listener.onQuantityAdd(position)
            }
            binding.layoutQuantityControl.imageSub.setOnClickListener {
                listener.onQuantitySub(position)
            }
            binding.root.setOnClickListener { listener.onItemClick(cart.layanan, position) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Layanan?, position: Int)
        fun onQuantityAdd(position: Int)
        fun onQuantitySub(position: Int)
    }
}
