package com.example.detergentapp.ui.main.order

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.databinding.ItemCartLayananBinding

import com.example.detergentapp.entity.DetailTransaksiLayanan
import timber.log.Timber


class LayananTrackingAdapter(
    private val context: Context,
    private val cartItemList: List<DetailTransaksiLayanan?>?,
) : RecyclerView.Adapter<LayananTrackingAdapter.DetailTransaksiLayananItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailTransaksiLayananItemViewHolder {
        val binding: ItemCartLayananBinding =
            ItemCartLayananBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailTransaksiLayananItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DetailTransaksiLayananItemViewHolder, position: Int) {
        holder.bind(cartItemList?.get(position), holder.adapterPosition)
    }

    override fun getItemCount(): Int {
        return cartItemList?.size ?: 0
    }

    class DetailTransaksiLayananItemViewHolder(var binding: ItemCartLayananBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(layananDetail: DetailTransaksiLayanan?, position: Int) {
            Timber.d("bind: $layananDetail")
            binding.textLayananName.text =
                "${if (layananDetail?.layanan?.satuanHitung == "Kg") layananDetail.berat else layananDetail?.satuan}X ${layananDetail?.layanan?.nama}"
            binding.textHargaLayanan.text = "Rp. ${layananDetail?.layanan?.harga}"
            binding.textJenisSatuan.visibility = ViewGroup.GONE
            binding.layoutQuantityControl.root.visibility = ViewGroup.GONE
        }
    }


}
