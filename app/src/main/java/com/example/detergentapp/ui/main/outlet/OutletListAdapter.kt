package com.example.detergentapp.ui.main.outlet

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.detergentapp.R
import com.example.detergentapp.databinding.ItemOutletGridBinding
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.utils.AppConstants
import com.squareup.picasso.Picasso

class OutletListAdapter(
    private val context: Context,
    private val outletItemList: List<Outlet>,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<OutletListAdapter.OutletItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OutletItemViewHolder {
        val binding: ItemOutletGridBinding =
            ItemOutletGridBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OutletItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OutletItemViewHolder, position: Int) {
        holder.bind(outletItemList[position], holder.adapterPosition, listener as OnItemClickListener)
    }

    override fun getItemCount(): Int {
        return outletItemList.size
    }

    class OutletItemViewHolder(var binding: ItemOutletGridBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(outlet: Outlet, position: Int, listener: OnItemClickListener) {
            Picasso.get().load("${AppConstants.URL_IMAGE_OUTLET}${outlet.foto}").fit()
                .placeholder(R.drawable.bg_rounded_grey)
                .into(binding.imageOutletList)
            binding.textNamaOutletList.text = outlet.nama
            binding.textAlamatOutletLIst.text = outlet.lokasi
            binding.outletItem.setOnClickListener { listener.onItemClick(position) }
            binding.buttonOrder.setOnClickListener { listener.onOrderClick(position) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
        fun onOrderClick(position: Int)
    }
}
