package com.example.detergentapp.ui.main.outlet

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.R
import com.example.detergentapp.databinding.FragmentOutletBinding
import com.example.detergentapp.databinding.MapsAnotationBinding
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.ui.auth.LoginActivity
import com.example.detergentapp.ui.transaction.TransactionActivity
import com.example.detergentapp.utils.LocationPermissionHelper
import com.mapbox.android.gestures.MoveGestureDetector
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.Style
import com.mapbox.maps.dsl.cameraOptions
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.MapAnimationOptions.Companion.mapAnimationOptions
import com.mapbox.maps.plugin.animation.flyTo
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.OnMoveListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorBearingChangedListener
import com.mapbox.maps.plugin.locationcomponent.OnIndicatorPositionChangedListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.plugin.locationcomponent.location2
import com.mapbox.maps.viewannotation.viewAnnotationOptions
import timber.log.Timber
import java.lang.ref.WeakReference


class OutletFragment : Fragment() {
    lateinit var binding: FragmentOutletBinding
    lateinit var outletAdapter: OutletListAdapter
    lateinit var viewModel: OutletViewModel
    private var outletList: ArrayList<Outlet> = ArrayList()
    private lateinit var mapView: MapView
    lateinit var userLocation: Point

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOutletBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
    }

    private fun observer() {
        viewModel = ViewModelProvider(this).get(OutletViewModel::class.java)
        viewModel.getOutletList.observe(this) {
            if (it == null) {
                Toast.makeText(requireContext(), "No result found", Toast.LENGTH_LONG).show()
            } else {
                outletList.clear()
                it.let { it1 -> outletList.addAll(it1) }
                outletAdapter.notifyDataSetChanged()
                mapView.getMapboxMap().loadStyleUri(
                    Style.MAPBOX_STREETS
                ) {
                    outletList.forEach { outlet ->
                        addAnnotationToMap(Point.fromLngLat(outlet.longitude, outlet.latitude))
                        val viewAnnotationManager = binding.mapView.viewAnnotationManager
                        val asyncInflater by lazy { AsyncLayoutInflater(requireContext()) }
                        viewAnnotationManager.addViewAnnotation(
                            resId = R.layout.maps_anotation,
                            options = viewAnnotationOptions {
                                geometry(Point.fromLngLat(outlet.longitude, outlet.latitude))
                                // move view annotation to the bottom on 20 pixels
                                offsetY(50)

                            },
                            asyncInflater = asyncInflater
                        ) { viewAnnotation ->
                            viewAnnotation.findViewById<TextView>(R.id.annotation).text = outlet.nama
                            viewAnnotation.setOnClickListener {
                                prefs?.saveOutlet(outlet)
                                val intent = Intent(requireContext(), TransactionActivity::class.java)
                                startActivity(intent).also { activity?.finish() }
                            }
                        }
                    }
                }
            }
        }
        viewModel.outletList()
    }

    private fun initView() {
        mapView = binding.mapView
        locationPermissionHelper = LocationPermissionHelper(WeakReference(requireActivity()))
        locationPermissionHelper.checkPermissions {
            onMapReady()
        }
        setupRecycleView()
    }


    private fun setupRecycleView() {
        outletAdapter = OutletListAdapter(requireContext(), outletList, object : OutletListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                mapView.location
                    .removeOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
                mapView.getMapboxMap().flyTo(
                    cameraOptions {
                        center(
                            Point.fromLngLat(
                                outletList[position].longitude,
                                outletList[position].latitude
                            )
                        ) // Sets the new camera position on click point
                        zoom(20.0) // Sets the zoom
                        bearing(180.0) // Rotate the camera
//                        pitch(50.0) // Set the camera pitch
                    },
                    mapAnimationOptions {
                        duration(7000)
                    }
                )
                Timber.d("outlet :${outletList[position]}")
            }


            override fun onOrderClick(position: Int) {
                prefs?.saveOutlet(outletList[position])
                val intent = Intent(requireContext(), TransactionActivity::class.java)
                startActivity(intent).also { activity?.finish() }
            }

        })
        binding.rvOutlet.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL, false)
        binding.rvOutlet.adapter = outletAdapter
    }

    //    mapbox
//    inisialisasi variabel
    private lateinit var locationPermissionHelper: LocationPermissionHelper

    private val onIndicatorBearingChangedListener = OnIndicatorBearingChangedListener {
        mapView.getMapboxMap().setCamera(CameraOptions.Builder().bearing(it).build())
    }


    private val onIndicatorPositionChangedListener = OnIndicatorPositionChangedListener {
        mapView.getMapboxMap().setCamera(CameraOptions.Builder().center(it).build())
        mapView.gestures.focalPoint = mapView.getMapboxMap().pixelForCoordinate(it)
        userLocation = it
    }

    private val onMoveListener = object : OnMoveListener {
        override fun onMoveBegin(detector: MoveGestureDetector) {
            onCameraTrackingDismissed()
        }

        override fun onMove(detector: MoveGestureDetector): Boolean {
            return false
        }

        override fun onMoveEnd(detector: MoveGestureDetector) {}
    }

    //    function
    private fun onMapReady() {
        mapView.getMapboxMap().setCamera(
            CameraOptions.Builder()
                .zoom(14.0)
                .build()
        )
        mapView.getMapboxMap().loadStyleUri(
            Style.MAPBOX_STREETS
        ) {
            initLocationComponent()
            setupGesturesListener()
        }
        binding.buttonLocation.setOnClickListener {
            mapView.location.addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
            mapView.location.addOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        }

    }

    private fun setupGesturesListener() {
        mapView.gestures.addOnMoveListener(onMoveListener)
    }

    private fun initLocationComponent() {
        val locationComponentPlugin = mapView.location
        locationComponentPlugin.updateSettings {
            this.enabled = true
            this.locationPuck = LocationPuck2D(
                bearingImage = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.mapbox_user_puck_icon,
                ),
                shadowImage = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.mapbox_user_icon_shadow,
                ),
                scaleExpression = interpolate {
                    linear()
                    zoom()
                    stop {
                        literal(0.0)
                        literal(0.6)
                    }
                    stop {
                        literal(20.0)
                        literal(1.0)
                    }
                }.toJson()
            )
        }
        locationComponentPlugin.addOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        locationComponentPlugin.addOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
    }

    private fun onCameraTrackingDismissed() {
        Toast.makeText(requireContext(), "onCameraTrackingDismissed", Toast.LENGTH_SHORT).show()
        mapView.location
            .removeOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        mapView.location
            .removeOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        mapView.gestures.removeOnMoveListener(onMoveListener)
    }


    override fun onDestroy() {
        super.onDestroy()
        mapView.location
            .removeOnIndicatorBearingChangedListener(onIndicatorBearingChangedListener)
        mapView.location
            .removeOnIndicatorPositionChangedListener(onIndicatorPositionChangedListener)
        mapView.gestures.removeOnMoveListener(onMoveListener)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    //    marker
    private fun addAnnotationToMap(point: Point) {
        bitmapFromDrawableRes(
            requireContext(),
            R.drawable.ic_laundry_location
        )?.let {
            val annotationApi = mapView.annotations
            val pointAnnotationManager = annotationApi.createPointAnnotationManager(mapView!!)
            val pointAnnotationOptions: PointAnnotationOptions = PointAnnotationOptions()
                .withPoint(point)
                .withIconImage(it)
            pointAnnotationManager.create(pointAnnotationOptions)
        }
    }

    private fun bitmapFromDrawableRes(context: Context, @DrawableRes resourceId: Int) =
        convertDrawableToBitmap(AppCompatResources.getDrawable(context, resourceId))

    private fun convertDrawableToBitmap(sourceDrawable: Drawable?): Bitmap? {
        if (sourceDrawable == null) {
            return null
        }
        return if (sourceDrawable is BitmapDrawable) {
            sourceDrawable.bitmap
        } else {
            val constantState = sourceDrawable.constantState ?: return null
            val drawable = constantState.newDrawable().mutate()
            val bitmap: Bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth, drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            bitmap
        }
    }

}
