package com.example.detergentapp

import android.app.Application
import android.content.Context
import android.location.Location
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.detergentapp.data.local.PreferencesHelper
import com.example.detergentapp.utils.ReleaseTree
import com.google.firebase.messaging.FirebaseMessaging
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import mumayank.com.airlocationlibrary.AirLocation
import timber.log.Timber
import timber.log.Timber.DebugTree


open class App : Application() {
    companion object {
        var prefs: PreferencesHelper? = null
        lateinit var instance: App
        var deviceId: String? = null
        var tokenFCM: String? = null
        fun alertDialog(context: Context, message: String = "Apakah kamu yakin?", action: () -> Unit) {
            AlertDialog.Builder(context)
                .setTitle(message)
                .setPositiveButton("Ya") { dialog, which ->
                    action()
                }
                .setNegativeButton("Tidak") { dialog, which ->
                    dialog.dismiss()
                }.show()
        }

    }

    override fun onCreate() {
        super.onCreate()
        deviceId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        FirebaseMessaging.getInstance().token.addOnSuccessListener() { instanceIdResult ->
            val newToken: String = instanceIdResult.toString()
            tokenFCM = newToken
            prefs?.tokenFCM = newToken
            Timber.d("newToken: $newToken")
        }.addOnFailureListener {
            Timber.e("error: ${it.message}")
        }
        instance = this
        prefs = PreferencesHelper(applicationContext)
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }
}
