package com.example.detergentapp.data.retrofit

import com.example.detergentapp.utils.AppConstants.URL
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


class ApiConfig {
    companion object {
        fun getRetroInstance(): Retrofit {
            val logging = HttpLoggingInterceptor()
            logging.level = (HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
            client.addInterceptor(logging)

            var gson = GsonBuilder()
                .setLenient()
                .serializeNulls()
                .create()

            return Retrofit.Builder()
                .baseUrl("${URL}api/")
                .client(client.build())
//                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
    }


}
