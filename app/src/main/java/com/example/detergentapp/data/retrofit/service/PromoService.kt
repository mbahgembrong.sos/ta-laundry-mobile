package com.example.detergentapp.data.retrofit.service

import com.example.detergentapp.entity.Promo
import com.example.detergentapp.entity.ResponseData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface PromoService {
    @GET("promo")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getPromoList(@Query("search") search: String = ""): Call<ResponseData<List<Promo>>>

    @GET("promo/{kode}")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getPromoBy(@Path("kode") kode: String): Call<ResponseData<Promo>>
}
