package com.example.detergentapp.data.local

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.utils.AppConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import timber.log.Timber

class PreferencesHelper(context: Context) : AppPreferencesHelper {
    private val loginPreferences: SharedPreferences =
        context.getSharedPreferences(AppConstants.LOGIN_PREFS, MODE_PRIVATE)
    private val cartPreferences: SharedPreferences =
        context.getSharedPreferences(AppConstants.CART_PREFERENCES, MODE_PRIVATE)
    override var id: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_ID, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_ID, value).apply()
    override var nama: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_NAMA, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_NAMA, value).apply()
    override var email: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_EMAIL, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_EMAIL, value).apply()
    override var jenisKelamin: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_JENIS_KELAMIN, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_JENIS_KELAMIN, value).apply()
    override var foto: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_FOTO, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_FOTO, value).apply()
    override var telp: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_TELP, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_TELP, value).apply()
    override var alamat: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_ALAMAT, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_ALAMAT, value).apply()
    override var password: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_PASSWORD, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_PASSWORD, value).apply()
    override var longtitude: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_LONGTITUDE, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_LONGTITUDE, value).apply()
    override var latitude: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_LATITUDE, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_LATITUDE, value).apply()
    override var tokenFCM: String?
        get() = loginPreferences.getString(AppConstants.CUSTOMER_TOKEN_FCM, null)
        set(value) = loginPreferences.edit().putString(AppConstants.CUSTOMER_TOKEN_FCM, value).apply()
    override var outlet: String?
        get() = (cartPreferences.getString(AppConstants.CART_OUTLET, null))
        set(value) = cartPreferences.edit().putString(AppConstants.CART_OUTLET, value.toString()).apply()
    override var cartItems: String?
        get() = cartPreferences.getString(AppConstants.CART_ITEMS, null)
        set(value) = cartPreferences.edit().putString(AppConstants.CART_ITEMS, value).apply()

    override fun saveUser(pelanggan: Pelanggan) {
        loginPreferences.edit().putString(AppConstants.CUSTOMER_ID, pelanggan.id).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_NAMA, pelanggan.nama).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_EMAIL, pelanggan.email).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_JENIS_KELAMIN, pelanggan.jenisKelamin).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_FOTO, pelanggan.foto).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_TELP, pelanggan.telp).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_ALAMAT, pelanggan.alamat).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_PASSWORD, pelanggan.password).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_LONGTITUDE, pelanggan.longtitude.toString()).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_LATITUDE, pelanggan.latitude.toString()).apply()
        loginPreferences.edit().putString(AppConstants.CUSTOMER_TOKEN_FCM, pelanggan.tokenFCM).apply()
    }


    override fun saveCart(cartItems: List<Cart>) {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val cartString = gson.toJson(cartItems)
        cartPreferences.edit().putString(AppConstants.CART_ITEMS, cartString).apply()
    }

    fun getCartItems(): List<Cart>? {
        val listType = object : TypeToken<List<Cart?>?>() {}.type
        return Gson().fromJson(cartItems, listType)
    }

    override fun saveOutlet(value: Outlet) {
        val gson = GsonBuilder().setPrettyPrinting().create()
        outlet = gson.toJson(value)
    }

    fun getOutletCart(): Outlet? {
        val type = object : TypeToken<Outlet?>() {}.type
        return Gson().fromJson(outlet, type)
    }

    override fun clearPreferences() {
        loginPreferences.edit().clear().apply()
        cartPreferences.edit().clear().apply()
    }

    override fun clearCartPreferences() {
        cartPreferences.edit().clear().apply()
    }


    fun getUser(): Pelanggan? {
        return Pelanggan(
            id,
            nama,
            email,
            jenisKelamin,
            foto,
            telp,
            alamat,
            password,
            if (!longtitude.isNullOrEmpty() && longtitude != "null") longtitude?.toDouble() else -7.81726720550563,
            if (!latitude.isNullOrEmpty() && latitude != "null") latitude?.toDouble() else 111.98783617101894,
            tokenFCM
        )
    }

}
