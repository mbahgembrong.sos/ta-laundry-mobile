package com.example.detergentapp.data.retrofit.service


import com.example.detergentapp.entity.*
import retrofit2.Call
import retrofit2.http.*

interface TransaksiService {
    @GET("transaksi/{id_pelanggan}")
    @Headers("Accept:*/*", "Content-Type:application/json")
    fun getTransaksiList(@Path("id_pelanggan") idPelanggan: String): Call<ResponseData<List<Transaksi>>>

    @GET("transaksi/show/{id_transaksi}")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getShowTransaksi(@Path("id_transaksi") idTransaksi: String): Call<ResponseData<Transaksi>>

    @POST("transaksi")
    fun postTambahTransaksi(
        @Body transaksi: TransaksiRequest
    ): Call<ResponseData<Transaksi>>

    @FormUrlEncoded
    @POST("transaksi/bayar/{id}")
    fun postBayarTransaksi(
        @Path("id") id: String,
        @Field("foto") foto: String,
        @Field("file") file: String,
    ): Call<ResponseData<Transaksi>>

    @FormUrlEncoded
    @POST("transaksi/status/{id}")
    fun postStatusTransaksi(
        @Path("id") id: String,
        @Field("status") status: String,
    ): Call<ResponseData<Transaksi>>

    @POST("ulasan")
    fun postTambahUlasan(
        @Body ulasan: Ulasan
    ): Call<ResponseData<Ulasan>>
}
