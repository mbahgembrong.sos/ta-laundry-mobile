package com.example.detergentapp.data.local

import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Outlet
import com.example.detergentapp.entity.Pelanggan
import com.google.gson.annotations.SerializedName

interface AppPreferencesHelper {
    var id: String?
    var nama: String?
    var email: String?
    var jenisKelamin: String?
    var foto: String?
    var telp: String?
    var alamat: String?
    var longtitude: String?
    var latitude: String?
    var tokenFCM: String?
    var password: String?
    var outlet: String?
    var cartItems: String?
    fun saveUser(pelanggan: Pelanggan)
    fun saveCart(cartList: List<Cart>)
    fun saveOutlet(cartList: Outlet)

    fun clearPreferences()

    fun clearCartPreferences()
}
