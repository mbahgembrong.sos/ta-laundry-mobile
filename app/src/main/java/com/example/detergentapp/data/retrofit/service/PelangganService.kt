package com.example.detergentapp.data.retrofit.service

import com.example.detergentapp.entity.Pelanggan
import com.example.detergentapp.entity.ResponseData
import retrofit2.Call
import retrofit2.http.*

interface PelangganService {

    @FormUrlEncoded
    @POST("pelanggan/register")
    fun postPelangganRegister(
        @Field("nama") nama: String,
        @Field("email") email: String,
        @Field("jenis_kelamin") jenisKelamin: String,
        @Field("telp") telp: String,
        @Field("alamat") alamat: String,
        @Field("password") password: String?,
        @Field("foto") foto: String?,
        @Field("longitude") longitude: Double?,
        @Field("latitude") latitude: Double?,
        @Field("token_fcm") tokenFcm: String?,
        @Field("file") status: String?
    ): Call<ResponseData<Pelanggan>>

    @FormUrlEncoded
    @POST("pelanggan/login")
    fun postPelangganLogin(
        @Field("email") email: String,
        @Field("password") password: String?,
    ): Call<ResponseData<Pelanggan>>

    @FormUrlEncoded
    @POST("pelanggan/token_fcm/{id}")
    fun postPelangganTokenFcm(
        @Field("token_fcm") tokenFcm: String,
        @Path("id") id: String,
    ): Call<ResponseData<Pelanggan>>

    @FormUrlEncoded
    @POST("pelanggan/update/{id}")
    fun postPelangganUpdate(
        @Path("id") id: String,
        @Field("nama") nama: String,
        @Field("email") email: String,
        @Field("jenis_kelamin") jenisKelamin: String,
        @Field("telp") telp: String,
        @Field("alamat") alamat: String,
        @Field("password") password: String?,
        @Field("foto") foto: String?,
        @Field("longitude") longitude: Double?,
        @Field("latitude") latitude: Double?,
        @Field("token_fcm") tokenFcm: String?,
        @Field("file") status: String?
    ): Call<ResponseData<Pelanggan>>

    @GET("pelanggan/{id}")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getAuthById(@Path("id") id: String): Call<ResponseData<Pelanggan>>

}
