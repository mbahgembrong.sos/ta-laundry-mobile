package com.example.detergentapp.data.retrofit.service

import com.example.detergentapp.entity.Layanan
import com.example.detergentapp.entity.ResponseData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface LayananService {
    @GET("layanan")
    @Headers("Accept:application/json", "Content-Type:application/json")
    fun getLayananList(): Call<ResponseData<List<Layanan>>>
}
