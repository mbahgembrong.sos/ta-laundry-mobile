package com.example.detergentapp.utils

import com.example.detergentapp.App.Companion.prefs
import com.example.detergentapp.entity.Cart
import com.example.detergentapp.entity.Layanan


class CartHelper {
    companion object {
        fun addItem(cartItem: Cart) {
            var cart: MutableList<Cart> = if (prefs?.getCartItems().isNullOrEmpty())
                mutableListOf()
            else
                prefs?.getCartItems() as MutableList<Cart>
            val targetItem = cart.singleOrNull { it.layanan.id == cartItem.layanan.id }
            if (targetItem == null) {
                cartItem.satuan++
                cartItem.totalHarga = cartItem.satuan * cartItem.layanan.harga
                cart.add(cartItem)
            } else {
                targetItem.satuan++
                targetItem.totalHarga = targetItem.satuan * targetItem.layanan.harga
            }
            prefs?.saveCart(cart)
        }

        fun showItem(layanan: Layanan): Cart? {
            var cart: MutableList<Cart> = if (prefs?.getCartItems().isNullOrEmpty())
                mutableListOf()
            else
                prefs?.getCartItems() as MutableList<Cart>
            return cart.singleOrNull { it.layanan.id == layanan.id }
        }

        fun removeItem(cartItem: Cart) {
            val cart = prefs?.getCartItems() as MutableList<Cart>
            val targetItem = cart.singleOrNull { it.layanan.id == cartItem.layanan.id }
            if (targetItem != null) {
                if (targetItem.satuan > 1) {
                    targetItem.satuan--
                    targetItem.totalHarga = targetItem.satuan * targetItem.layanan.harga
                } else {
                    cart.remove(targetItem)
                }
            }
            prefs!!.saveCart(cart)
        }


        fun getShoppingCartSize(): Int {
            var cartSize = 0
            prefs?.getCartItems()?.forEach {
                cartSize += it.satuan;
            }
            return cartSize
        }

        fun getShoppingTotal(): Int {
            var cartGrandTotal = 0
            prefs?.getCartItems()?.forEach {
                cartGrandTotal += it.totalHarga;
            }
            return cartGrandTotal
        }
    }

}
