package com.example.detergentapp.utils

object AppConstants {

    const val URL = "http://192.168.99.1/"
    const val URL_IMAGE_PELANGGAN = URL + "storage/pelanggan/"
    const val URL_IMAGE_LAYANAN = URL + "storage/layanan/"
    const val URL_IMAGE_OUTLET = URL + "storage/outlet/"
    const val URL_IMAGE_PROMO = URL + "storage/promo/"

    //PREFS
    const val LOGIN_PREFS = "LOGIN_PREFS"
    const val CART_PREFERENCES = "CART_PREFERENCES"
    const val CUSTOMER_ID = "CUSTOMER_ID"
    const val CUSTOMER_NAMA = "CUSTOMER_NAMA"
    const val CUSTOMER_EMAIL = "CUSTOMER_EMAIL"
    const val CUSTOMER_JENIS_KELAMIN = "CUSTOMER_JENIS_KELAMIN"
    const val CUSTOMER_FOTO = "CUSTOMER_FOTO"
    const val CUSTOMER_TELP = "CUSTOMER_TELP"
    const val CUSTOMER_ALAMAT = "CUSTOMER_ALAMAT"
    const val CUSTOMER_PASSWORD = "CUSTOMER_PASSWORD"
    const val CUSTOMER_LONGTITUDE = "CUSTOMER_LONGTITUDE"
    const val CUSTOMER_LATITUDE = "CUSTOMER_LATITUDE"
    const val CUSTOMER_TOKEN_FCM = "CUSTOMER_TOKEN_FCM"
    const val CART_OUTLET = "CART_OUTLET"
    const val CART_ITEMS = "CART_ITEMS"

    //REQUEST INTENT
    const val INTENT_TRANSAKSI_ID = "INTENT_TRANSAKSI_ID"
    const val NOTIFICATION_TITLE = "NOTIFICATION_TITLE"

}
